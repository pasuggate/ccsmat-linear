{-# LANGUAGE FlexibleContexts #-}
module Main where

import System.Random
import Control.Monad.ST
import Control.Monad.IO.Class
import Data.Vector.Extras hiding (vec)
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Generic.Mutable as Mut
import Criterion.Main (defaultMain, bench, bgroup, whnf, Benchmark)
import Linear (V1 (..))
import Text.Printf

import Data.CCSMat
import Data.CCSMat.MapMat
import Data.CCSMat.Vector
import Data.CCSMat.VectorSpace
import Data.CCSMat.Stream


mkCCS :: Int -> IO (CCSMat R)
mkCCS n = do
  (gi, gf) <- split <$> getStdGen
  let go     _      _  0 mm = mm
      go (j:js) (x:xs) c mm = go js xs (c-1) $ mmInsert i k x mm where
        (i, k) = (div j n `mod` n, j `mod` n)
      -- initial matrix, and number-of-non-zeroes:
      m0 = toMapMat $ eye n :: MapMat R
      n' = (n*) . round . (sqrt :: R -> R) $ fromIntegral n :: Z
      -- some random numbers:
      xi = abs <$> randoms gi
      xf = randoms gf
  pure . fromMapMat n n $ go xi xf n' m0

spmvBench :: CCSMat R -> Vector R -> Vector (V1 R) -> Benchmark
spmvBench mat vec vx1 =
  bgroup "SpMV operations"
  [ bench "spmv  (R)   " $ whnf (spmv  mat) vec
  , bench "spmvs (R)   " $ whnf (spmvs mat) vec
  , bench "spmv  (V1 R)" $ whnf (spmv  mat) vx1
  , bench "spmvs (V1 R)" $ whnf (spmvs mat) vx1
  ]

main :: IO ()
main  = do
  let vec = fromIntegral `Vec.map` (efn 1 20 :: Vector Z) :: Vector R
      vx1 = Vec.map V1 vec
  mat <- mkCCS 20
{-- }
  printf "Matrix:\n%s\n" $ show mat
  printf "Vector:\n%s\n" $ show vec
  printf "Product:\n%s\n" . show $ spmv mat vec
  printf "Produkt:\n%s\n" . show $ spmv mat vx1
  printf "Prodavt:\n%s\n" . show $ spmvs mat vx1
--}

  defaultMain
    [ spmvBench mat vec vx1
    ]
