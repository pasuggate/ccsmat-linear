{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, TupleSections,
             FlexibleContexts, BangPatterns, RecordWildCards, OverloadedStrings,
             MultiParamTypeClasses, FunctionalDependencies,
             FlexibleInstances, TypeSynonymInstances, InstanceSigs,
             GeneralizedNewtypeDeriving, StandaloneDeriving,
             DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric,
             GADTs, KindSignatures, DataKinds, PolyKinds,
             TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances,
             CPP, TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Constrain
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Build Lagrange-multiplier constraints into (typically) a stiffness matrix.
--
-- NOTE:
-- 
-- Changelog:
--  + 18/05/2017  -- initial file;
-- 
-- TODO:
--  + filter-out any zeroes, in the given vector?
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Constrain
       ( constrainWith
       ) where

import Control.Monad.ST
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable         as S
import qualified Data.Vector.Storable.Mutable as M
import Data.Vector.Helpers hiding (vec)

import Data.CCSMat.Internal


-- * Augments a system-matrix with additional constraints.
------------------------------------------------------------------------------
-- | Augment a system-matrix with an additional constraint.
constrainWith :: (Storable a, Num a) => CCSMat a -> Vector a -> CCSMat a
{-# SPECIALISE constrainWith :: CCSD -> DVec -> CCSD #-}
constrainWith mat vec
  | n /= m || n /= o = error $ modName ++ ".constrainWith: dimensions don't match"
  | otherwise        = cxWith mat vec
  where ((m, n), o)  = (getDims mat, len vec)

{-- }
-- | Augment a system-matrix with additional constraints.
--   TODO:
constrainWithN :: (Storable a, Num a) => CCSMat a -> [Vector a] -> CCSMat a
{-# SPECIALISE constrainWithN :: CCSD -> [DVec] -> CCSD #-}
constrainWithN mat vec = undefined
--}


-- ** Internal helper-functions.
------------------------------------------------------------------------------
modName :: String
modName  = "CCSMat.Constrain"

--   TODO: Filter-out any zeroes, in the given vector?
cxWith :: (Storable a, Num a) => CCSMat a -> Vector a -> CCSMat a
{-# INLINE[1] cxWith #-}
cxWith (CCS rn ri cp xs) ys = runST $ do
  let n' = rn+1
      nz = len xs + 2*len ys
      cn = unscanlSum cp
  rr <- M.new nz
  cc <- M.new (len cp+1)
  xx <- M.new nz
  let cols j
        | j < len cp = wr cc j (cp!j+j) *> cols (j+1)
        | otherwise  = wr cc j nz
      vals q j
        | j <   rn   = do
          let (n, p) = (cn!j, cp!j)
              upd i  = wr rr (q+i) (ri!(p+i)) *> wr xx (q+i) (xs!(p+i))
          upd `S.mapM_` S.enumFromN 0 n
          wr rr (q+n) rn *> wr xx (q+n) (ys!j) *> vals (q+n+1) (j+1)
        | otherwise  = let upd i  = wr rr (q+i) i *> wr xx (q+i) (ys!i)
                       in  upd `S.mapM_` S.enumFromN 0 rn
  cols 0 *> vals 0 0
  CCS n' <$> frz rr <*> frz cc <*> frz xx
