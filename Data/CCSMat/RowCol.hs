{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, TupleSections #-}
{-# LANGUAGE FlexibleContexts, BangPatterns #-}
{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances, TypeSynonymInstances #-}
-- {-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric #-}
{-# LANGUAGE GADTs, KindSignatures, DataKinds, PolyKinds #-}
{-# LANGUAGE TypeFamilies, RankNTypes, ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TemplateHaskell #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.RowCol
-- Copyright   : (C) Patrick Suggate 2016
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Row and column operations for Compressed-Column Sparse (CCS) matrices.
-- 
-- Changelog:
--  + 20/04/2016  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.CCSMat.RowCol where

-- import Foreign (Storable(..), castPtr, plusPtr)
-- import GHC.Types (SPEC(..))
import Control.Monad.ST
import System.IO.Unsafe (unsafePerformIO)
import Data.Bool
import Data.Vector.Helpers
import Data.Vector.Storable (Storable, Vector)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Storable.Mutable as Mut
import Text.Printf

import Data.Vector.Sparse (SparseVector (..))
import Data.CCSMat.Internal
import Data.CCSMat.Vector (sparseCol)


------------------------------------------------------------------------------
-- | Extract a sparse row-vector.
--   NOTE: O(n+m) -- where `n` is the number of "nodes," and `m` is the number
--     of "edges."
sparseRow :: Storable a => Int -> CCSMat a -> SparseVector a
{-# SPECIALIZE sparseRow :: Int -> CCSD -> SparseVector R #-}
sparseRow i (CCS rn ri cp xs)
  | i  >=  rn = error $ printf "RowCol.sparseRow: matrix doesn't contain row %d" i
  | otherwise = runST $ do
    -- ^ count the number of non-zero elements in row `i`:
    let ps = Vec.elemIndices i ri
        nz = len ps

    -- ^ construct new arrays for the indices and values:
    cr <- new nz
    xr <- new nz

    -- ^ find which columns the row-index pointers belong to:
    --   Inputs:
    --    q  --  destination pointer;
    --    j  --  current column; and
    --    k  --  index into the array of row-element pointers.
    let go q j k | k >=  nz  = return ()
                 | p > cp!j' = go q j' k
                 | otherwise = wr cr q j *> wr xr q (xs!p) *> go q' j' k'
          where
            (p, q', j', k')  = (ps!k, q+1, j+1, k+1)

    go 0 0 0
    SparseVector (len cp - 1) <$> frz cr <*> frz xr


------------------------------------------------------------------------------
-- | Add the given column vector to a sparse matrix.
--   NOTE: Only works for sorted columns.
--   
--   TODO: Needs to work for both sorted and unsorted CCS matrices.
addCol :: (Storable a, Num a) => Int -> SparseVector a -> CCSMat a -> CCSMat a
{-# SPECIALIZE addCol :: Int -> SparseVector R -> CCSD -> CCSD #-}
addCol j (SparseVector en is xs) (CCS rn ri cp ys)
  | en /= rn  = error "RowCol.addCol: dimensions don't match"
  | otherwise = runST $ do
    -- ^ count the number of new matrix elements:
    --   TODO: compute where to update/insert the new values?
    fr <- new en
    Mut.set fr 0
    let (p, q, j') = (cp!j, cp!j', j+1)
        tic = wr fr `flip` 1
        ks  = slc p (q - p) ri
    Vec.mapM_ tic is *> Vec.mapM_ tic ks
    nj <- Vec.sum <$> frz fr

    -- ^ construct new arrays for the column-pointers, indices, and values:
    let nz = len ri + nn
        nn = nj - len ks
    cc <- thw (unscanlSum cp) >>= \ar -> wr ar j nj *> frz ar
    rr <- new nz
    xr <- new nz

    -- ^ perform a sparse-vector addition:
    --   inputs:
    --    `s` current sparse-vector index?
    --    `t` source sparse-matrix index?
    --    `r` destination sparse-matrix index?
    let fll s t r
          | s == len is = cpy r t (q - t)
          | i == k      = wr xr r z *> wr rr r k *> fll s' t' r'
          | i >  k      = wr xr r y *> wr rr r k *> fll s  t' r'
          | otherwise   = wr xr r x *> wr rr r i *> fll s' t  r'
          where
            (x, y, z, i, k) = (xs!s, ys!t, x + y, is!s, ri!t)
            (s', t', r') = (s+1, t+1, r+1)
        -- ^ "memcpy" `n` words from `*p` to `*q`:
        cpy q p n
          | n   >   0 = wr xr q (ys!p) *> wr rr q (ri!p) *> cpy (q+1) (p+1) (n-1)
          | otherwise = return q

    cpy 0 0 p
    fll 0 p p
    cpy (p+nj) q (len ri - q)
    CCS rn `flip` Vec.scanl' (+) 0 cc <$> frz rr <*> frz xr

addColNNZ :: Z -> SparseVector a -> CCSMat a -> Z
addColNNZ j (SparseVector vn vi _) (CCS rn ri cp _) = runST $ do
  fr <- new vn
  Mut.set fr 0
  let (p, q, j') = (cp!j, cp!j', j+1)
      tic = wr fr `flip` 1
      ks  = slc p (q - p) ri
  Vec.mapM_ tic vi *> Vec.mapM_ tic ks
  Vec.sum <$> frz fr


------------------------------------------------------------------------------
-- | Add the given row-vector to a sparse matrix.
--   TODO: Needs to work for both sorted and unsorted CCS matrices.
addRow :: (Storable a, Num a) => Int -> SparseVector a -> CCSMat a -> CCSMat a
{-# SPECIALIZE addRow :: Int -> SparseVector R -> CCSD -> CCSD #-}
addRow i (SparseVector cn js xs) (CCS rn ri cp ys)
  | cn /= len cp - 1 = error "RowCol.addRow: dimensions don't match"
--   | otherwise        = runST $ do
  | otherwise        = unsafePerformIO $ do
    -- ^ count the number of new matrix elements:
    --   TODO: compute where to update/insert the new values?
    let cnt ks | Vec.null ks             = 0
               | Vec.elem i (slc p n ri) = cnt ks'
               | otherwise               = 1 + cnt ks'
          where
            (k, ks') = (hed ks, tal ks)
            (p, n  ) = (cp!k, cp!(k+1) - p)
        nz  = cnt js + len ri

    -- ^ construct new arrays for the column-pointers, indices, and values:
    cr <- thw $ unscanlSum cp
    rr <- new nz
    xr <- new nz

    -- ^ set the array contents, by copying over the matrix values, while
    --   adding in any of the sparse-vector contributions:
    let fll p j q r
          | p >= len js = cpy r q (lst cp - q)
          | j >=  cn    = return q
          | j == js!p   = col p j q e r   >>= fll p' j' e
          | otherwise   = cpy r q (e - q) >>= fll p  j' e
          where
            (p',j',e) = (p+1, j+1, cp!j')

        -- ^ copy a column, adding the sparse-vector value if needed. Once the
        --   sparse-vector value has been used, then just "memcpy" across the
        --   rest of the matrix column values.
        col p j q e r
          | q  >=  e  = wr xr r x *> wr rr r i *> mfy cr (+1) j *> return r'
          | k  ==  i  = wr xr r z *> wr rr r i *> cpy r' q' (e - q')
          | k  >   i  = wr xr r x *> wr rr r i *> mfy cr (+1) j *> cpy r' q  (e - q)
          | otherwise = wr xr r y *> wr rr r k *> col p j q' e r'
          where
            (x, y, z) = (xs!p, ys!q, x + y)
            (q',r',k) = (q+1, r+1, ri!q)

        -- ^ "memcpy" `n` words from `*p` to `*q`:
        cpy q _ 0 = return q
        cpy q p n = wr xr q (ys!p) *> wr rr q (ri!p) *> cpy (q+1) (p+1) (n-1)

    fll 0 0 0 0
--     nn <- fll 0 0 0 0
--     whn (nn /= nz) $ error "addRow: not enough entries were written."
    CCS rn <$> frz rr <*> Vec.scanl' (+) 0 `fmap` frz cr <*> frz xr

------------------------------------------------------------------------------
-- | Drop the indicated columns from the given sparse matrix.
dropCols :: Storable a => [Int] -> CCSMat a -> CCSMat a
{-# SPECIALIZE dropCols :: [Int] -> CCSD -> CCSD #-}
dropCols js (CCS rn ri cp xs) = runST $ do
  let nz = len ri - sum (map (cc!) js)
      cc = unscanlSum cp
      cn = len cp - length js - 1

  -- ^ construct new arrays for the column-pointers, indices, and values:
  cr <- new (cn+1)
  rr <- new nz
  xr <- new nz

  let fll k l    []   q
        | l  >=  cn = wr cr l q
        | otherwise = wr cr l q >> cpy q (cp!k) (cc!k) >>= fll (k+1) (l+1) []
      fll k l (j:js') q
        | k  ==  j  = fll k' l js' q
        | otherwise = wr cr l q >> cpy q p (cc!k) >>= fll k' l' (j:js')
        where
          (p, k', l') = (cp!k, k+1, l+1)

      -- ^ "memcpy" `n` words from `*p` to `*q`:
      cpy q _ 0 = return q
      cpy q p n = wr xr q (xs!p) *> wr rr q (ri!p) *> cpy (q+1) (p+1) (n-1)

  fll 0 0 js 0
  CCS rn <$> frz rr <*> frz cr <*> frz xr

{-- }
------------------------------------------------------------------------------
-- | Drop the indicated columns from the given sparse matrix.
--   TODO:
dropRows :: Storable a => [Int] -> CCSMat a -> CCSMat a
dropRows is (CCS rn ri cp xs) = runST $ do
  let ps = Vec.concat $ Vec.elemIndices `flip` ri <$> is
      nz = len ri - len ps

  -- ^ construct new arrays for the column-pointers, indices, and values:
  cr <- thw $ unscanlSum cp
  rr <- new nz
  xr <- new nz

  let fll k l    []   q
        | l  >=  cn = wr cr l q
        | otherwise = wr cr l q >> cpy q (cp!k) (cc!k) >>= fll (k+1) (l+1) []
      fll k l (j:js') q
        | k  ==  j  = fll k' l js' q
        | otherwise = wr cr l q >> cpy q p (cc!k) >>= fll k' l' (j:js')
        where
          (p,k',l') = (cp!k, k+1, l+1)

      -- ^ "memcpy" `n` words from `*p` to `*q`:
      cpy q _ 0 = return q
      cpy q p n = wr xr q (xs!p) *> wr rr q (ri!p) *> cpy (q+1) (p+1) (n-1)

  fll 0 0 js 0
  CCS rn <$> frz rr <*> frz cr <*> frz xr
--}

------------------------------------------------------------------------------
-- | Zero all of the off-diagonal entries for the given vector of variable
--   indices.
elimVars :: (Storable a, Num a) => Vector Int -> CCSMat a -> CCSMat a
{-# SPECIALIZE elimVars :: Vector Int -> CCSD -> CCSD #-}
elimVars js (CCS rn ri cp xs) = runST $ do
  ar <- thw (Vec.replicate rn False :: Vector Bool)
  ij :: Vector Bool <- Vec.mapM_ (wr ar `flip` True) js *> frz ar

  -- ^ construct, and fill, a new array for the values:
  let nz = len ri
  xr <- Vec.thaw xs
  let col j q p
        | p  >=  nz = return ()
        | p  >=  q  = col (j+1) (cp!(j+2)) q
        | ij!j      = bool (wr xr p 1) (wr xr p 0) (i /= j) *> col j q p'
--         | ij!j      = whn (i /= j) (wr xr p 0) *> col j q p'
        | otherwise = whn (ij!i)   (wr xr p 0) *> col j q p'
        where
          (i, p') = (ri!p, p+1)

  col 0 (cp!1) 0
  CCS rn ri cp <$> frz xr

-- | This version removes the entries, instead of just zeroing them.
elimVars' :: (Storable a, Num a) => Vector Int -> CCSMat a -> CCSMat a
{-# SPECIALIZE elimVars' :: Vector Int -> CCSD -> CCSD #-}
elimVars' js (CCS rn ri cp xs) = runST $ do
  -- ^ construct, and fill, a new array for the values:
  let nz = len ri
      ij = rep rn False `upd_` js $ len js `rep` True
  cr <- new (len cp)
  Mut.set cr 0
  rr <- new nz
  xr <- new nz
  let col j q p r
        | p  >=  nz = return r
        | p  >=  q  = col (j+1) (cp!(j+2)) q r
        | ij!j      = bool (upd 1 i j r >>= col j q p') (col j q p' r) (i /= j)
        | ij!i      = col j q p' r
        | otherwise = upd x i j r >>= col j q p'
        where
          (i, x, p') = (ri!p, xs!p, p+1)
      upd x i j r = wr xr r x *> wr rr r i *> mfy cr (+1) j *> return (r+1)
      acc x j | j < len cp = rd cr j >>= \c -> wr cr j x *> acc (x+c) (j+1)
              | otherwise  = frz cr
  nn <- col 0 (cp!1) 0 0
  cq <- acc 0 0
  CCS rn `flip` cq <$> tak nn `fmap` frz rr <*> tak nn `fmap` frz xr

-- | Compute the new number of non-zeros, after eliminating the indicated
--   variables.
elimNNZ :: Vector Z -> CCSMat a -> Z
elimNNZ js (CCS rn ri cp _) =
  let ij = rep rn False `upd_` js $ len js `rep` True
      nz = len ri
      cnt j q p c
        | p  ==  nz    = c
        | p  ==  q     = cnt (j+1) (cp!(j+2)) q c
        | i  ==  j     = cnt j q p' c' -- ^ always store diagonal entries
        | ij!j || ij!i = cnt j q p' c
        | otherwise    = cnt j q p' c'
        where
          (i, c', p')  = (ri!p, c+1, p+1)
  in  cnt 0 (cp!1) 0 0

------------------------------------------------------------------------------
-- | Given the indices of two variables, assuming that `-x_i = x_j`, perform
--   row & column operations to merge variables, so that the `i` row & column
--   can be eliminated.
--   NOTE: Assumes that the given matrix is symmetric.
--   
--   TODO: Something smarter can be done, exploiting the symmetry of the
--     matrix?
substVar :: Z -> Z -> CCSMat R -> CCSMat R
substVar i j mtx =
--   let mtx' = addRow j (negate $ sparseRow i mtx) mtx
  let mtx' = addRow j (negate $ sparseCol mtx i) mtx
  in  addCol j (negate $ sparseCol mtx' i) mtx'
--   addCol j (negate $ sparseCol mtx i) mtx

-- | Add instead of subtract.
addstVar :: Z -> Z -> CCSMat R -> CCSMat R
addstVar i j mtx =
  let mtx' = addRow j (sparseRow i mtx) mtx
  in  addCol j (sparseCol mtx' i) mtx'
