{-# LANGUAGE BangPatterns, Rank2Types, GADTs, FlexibleContexts, DeriveFunctor,
             ScopedTypeVariables, TupleSections
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Bundle
-- Copyright   : (C) Patrick Suggate, 2015
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Stream-fusion functionality, for sparse matrices.
-- 
-- Changelog:
--  + 27/08/2015  --  initial file;
-- 
-- TODO:
--  + monadic streams;
--  + flattening of nested streams?
--  + set-ops; e.g., union, etc.?
--  + `SPEC` specialisations?
--  + look at (the fusion in) Kmett's `sparse` library;
--  + what use-cases allow for SIMD?
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Bundle
       ( Dims(..)
       , Bundle(..)
       , prodShapeS
       , saxpyS
       , prodShapE
       , testBundle
--        , foldStep, done
       ) where

import Prelude
import GHC.Types (SPEC(..))
import Control.Monad.Primitive
import Control.Monad.ST
import Control.Monad.Identity
import Data.Bool
import Data.Vector.Helpers
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Generic.Mutable as M
import Data.Vector.Storable (Vector, Storable)
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable as Vec
-- import qualified Data.Vector.Storable.Mutable as Mut

import Data.Vector.Fusion.Stream.Monadic (Stream(..), Step(..))
import qualified Data.Vector.Fusion.Stream.Monadic as Stream
import Data.Vector.Fusion.Bundle.Size

-- import Data.Vector.Sparse.Stream hiding (filtM)
import Data.Vector.Sparse
import qualified Data.Vector.Sparse.Stream as Sparse
import Data.CCSMat.Internal
import Data.CCSMat.Vector (sparseCol)
import Data.CCSMat.Stream
import Data.CCSMat.Multiply


-- * Types useful for bundling.
------------------------------------------------------------------------------
data Dims = Dims Z Z Size deriving (Eq, Show)

data Bundle m a =
  Bundle { bElements :: Stream m (Elem a)
         , bIndices  :: Stream m Ix
         , bVectors  :: Stream m (SparseVector a)
         , bMatrix   :: Maybe (CCSMat a)
         , bSize     :: Dims
         }


fromMatrix :: (Monad m, Storable a) => CCSMat a -> Bundle m a
fromMatrix m = Bundle (elemS m) (indiceS m) (colS m) (Just m) dims
  where
    dims = Dims (getRowN m) (getColN m) . Exact $ getNNZ m


mulB :: (Num a, Storable a, Monad m) => Bundle m a -> Bundle m a -> Bundle m a
mulB  = undefined


prodShapE :: CCSMat a -> CCSMat a -> IVec
prodShapE ma mb = runST $ prodShapeS ma mb

prodShapeS :: PrimMonad m => CCSMat a -> CCSMat a -> m IVec
{-# INLINE [1] prodShapeS #-}
prodShapeS ma@(CCS rn _ _ _) mb@(CCS _ _ cp _) = do
  flg <- G.unsafeThaw (G.replicate rn (-1) :: IVec)
  scanColsOfB flg (colIdxs ma) (len cp) (colIdxS mb)

scanColsOfB ::
  PrimMonad m => MVec m Z -> (Z -> IVec) -> Z -> Stream m IVec -> m IVec
{-# INLINE [1] scanColsOfB #-}
scanColsOfB flg f cc =
  let -- {-# INLINE go #-}
      go nz = uncurry (scanRowOfB flg f nz)
  in  unstream cc . Stream.scanlM' go 0 . Stream.indexed

scanRowOfB ::
  PrimMonad m => MVec m Z -> (Z -> IVec) -> Z -> Z -> IVec -> m Z
{-# INLINE [1] scanRowOfB #-}
scanRowOfB flg f nz j =
  let {-# INLINE go #-}
      go c = scanColOfA flg j . f >=> return . (c+)
  in  G.foldM' go nz

scanColOfA :: PrimMonad m => MVec m Z -> Z -> IVec -> m Z
{-# INLINE [1] scanColOfA #-}
scanColOfA flg j =
  let {-# INLINE go #-}
      go nz i =
        rd flg i >>= bool (const (nz+1) <$> wr flg i j) (return nz) . (==j)
  in  G.foldM' go 0

saxpyS ::
  (PrimMonad m, Num a, Storable a) => CCSMat a -> CCSMat a -> m (CCSMat a)
saxpyS ma@(CCS rn _ _ _) mb = do
  cp  <- prodShapeS ma mb
  let nz = lst cp
  rr  <- new nz
  xr  <- new nz
  flg <- thw (G.replicate rn (-1) :: IVec)
  flip (CCS rn) cp <$> frz rr <*> frz xr


-- * Testing and examples.
------------------------------------------------------------------------------
testBundle = do
  let ri = vec [0,0,1]
      cp = vec [0,1,3]
      xs = vec [1,2,3] :: Vector Double
      mm = CCS 2 ri cp xs
      rj = vec [0,1]
      cq = vec [0,2,2]
--       cq = vec [0,0,2]
      ys = vec [4,5] :: Vector Double
      qq = CCS 2 rj cq ys
  prodShapeS mm qq >>= print
  prodShapeS qq mm >>= print
  print $ prodShape mm qq
  print $ prodShape qq mm
