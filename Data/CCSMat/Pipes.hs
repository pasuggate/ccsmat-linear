{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE Rank2Types, GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ScopedTypeVariables #-}
-- {-# LANGUAGE NoMonomorphismRestriction #-}
-- {-# LANGUAGE LiberalTypeSynonyms, ImpredicativeTypes #-}
module Data.CCSMat.Pipes
       ( Tube, fuse, idT
       , trans, trilP
       , testPipes
       ) where

------------------------------------------------------------------------------
--
--  Stream-fusion functionality, for sparse matrices.
--
--  Changelog:
--   + 31/08/2015  --  initial file;
--
--  TODO:
--   + monadic streams;
--   + flattening of nested streams?
--   + set-ops; e.g., union, etc.?
--   + `SPEC` specialisations?
--   + look at (the fusion in) Kmett's `sparse` library;
--   + what use-cases allow for SIMD?
--

import Prelude hiding (Maybe(..))
import GHC.Types (SPEC(..))
import Control.Monad.Primitive
import Control.Monad.ST
import Control.Monad.Identity
-- import Data.Maybe
import Data.Vector.Helpers
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Generic.Mutable as M
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Storable.Mutable as Mut

import Data.Vector.Sparse
import Data.CCSMat.Internal


-- * Types useful for piping.
------------------------------------------------------------------------------
-- | Matrix elements.
data Elem a = Elem !Int !Int !a
-- data Elem a = Elem Int Int a
            deriving (Eq, Show, Functor)

type ElemD  = Elem Double

data Opt a = Val !a | Nil
           deriving (Eq, Show)

instance Functor Opt where
  {-# INLINE fmap #-}
  fmap f o = case o of
    Val x -> Val (f x)
    Nil   -> Nil

{--}
data Maybe a = Just !a | Nothing
             deriving (Eq, Show)

instance Functor Maybe where
  {-# INLINE fmap #-}
--   fmap f (Just x) = Just (f x)
--   fmap f  Nothing = Nothing
  fmap f o = case o of
    Just  x -> Just (f x)
    Nothing -> Nothing

fromJust :: Maybe a -> a
{-# INLINE fromJust #-}
fromJust (Just x) = x
fromJust  _       = error "Shit"
--}


data Tube i o m r = Exec (m (Tube i o m r))
                  | Grab (Maybe i -> Tube i o m r)
                  | Give (Tube i o m r) (Maybe o)
                  | Pure (Maybe r)

type Src   b m r = Tube ()  b m r
type Snk a   m r = Tube  a () m r
type Eff     m r = Tube () () m r


-- * Basic `Tube` functions.
------------------------------------------------------------------------------
runT :: Monad m => Src a m r -> m [a]
{-# INLINE [1] runT #-}
runT  = go SPEC
  where
    go !_ s = case s of
      Give sf (Just x) -> (x:) <$> go SPEC sf
      Give sf  Nothing -> go SPEC sf
      Exec kf          -> kf >>= go SPEC
      _                -> return []

runE :: Monad m => Eff m r -> m (Maybe r)
{-# INLINE [1] runE #-}
runE  = go SPEC
  where
    go !_ s = case s of
      Exec kf   -> kf >>= go SPEC
      Give sf _ -> go SPEC sf
      Pure mr   -> return mr
      _         -> return Nothing

fuse :: Functor m => Tube a b m r -> Tube b c m r -> Tube a c m r
{-# INLINE [1] fuse #-}
fuse  = g0 SPEC
  where
    g0 !_ up dn = case dn of
      Give d' o -> Give (g0 SPEC up d') o
      Grab d'   -> g1 SPEC up
        where
          g1 !_ u' = case u' of
            Give ux p -> g0 SPEC ux (d' p)
            Grab ux   -> Grab $ g1 SPEC . ux
            Exec k    -> Exec $ g1 SPEC <$> k
            Pure mr   -> Pure mr
      Exec k    -> Exec $ g0 SPEC up <$> k
      Pure mr   -> Pure mr

idT :: Tube i i m r
{-# INLINE [1] idT #-}
idT  = go SPEC
  where
    go !_ = Grab (Give (go SPEC))


-- * Additional functions.
------------------------------------------------------------------------------
-- | Filter a tube.
filts :: (a -> Bool) -> Tube a a m r
{-# INLINE filts #-}
filts f = Grab (go SPEC)
  where
    go !_ mx = case mx of
      Just  x -> let gg = Grab (go SPEC)
                 in  if f x then gg $< x else gg
--                  if f x then Grab (go SPEC) $< x else Grab (go SPEC)
      Nothing -> done

{-# INLINE done #-}
done = Give (Pure Nothing) Nothing

{-# INLINE final #-}
final = (Pure Nothing $<)

{-# INLINE inject #-}
inject = Pure . Just

{-# INLINE ($<) #-}
($<) = give

give :: Tube a b m r -> b -> Tube a b m r
{-# INLINE give #-}
give f = Give f . Just


-- * Matrix stuff.
------------------------------------------------------------------------------
elems :: Storable a => CCSMat a -> Src (Elem a) m r
{-# INLINE [1] elems #-}
elems (CCS rn ri cp xs) = go SPEC 0 0
  where
    go !_ !j !p | p  ==  q  = go SPEC j' p
                | p  <  nz  = go SPEC nj p' $< Elem (ri!p) j (xs!p)
                | otherwise = done
      where q  = cp!j'
            p' = succ p
            j' = succ j
            nj = if p' == q then j' else j
    nz = len ri

cptrs :: Tube (Elem a) Z m r
{-# INLINE [1] cptrs #-}
cptrs  = Give (go SPEC 0 0) (Just 0)
  where
    go !_ !k !p = Grab $ \me -> case me of
      Just (Elem _ j _) ->
        let dn !_ !l !q | j  ==  l  = go SPEC l  q'
                        | j  ==  l' = go SPEC l' q' $< q
                        | otherwise = dn SPEC l' q  $< q
              where q' = succ q
                    l' = succ l
        in  dn SPEC k p
      Nothing -> final p

------------------------------------------------------------------------------
-- | Count the number of elements within each column, for the given stream of
--   sparse-matrix elements.
ccnts :: forall v a m. (G.Vector v Z, PrimMonad m) =>
         Z -> Snk (Elem a) m (v Z)
{-# INLINE [1] ccnts #-}
ccnts cn = Exec $ do
  ar <- G.unsafeThaw (G.replicate cn 0 :: v Z)
  let go !_ me = case me of
        Just (Elem _ j _) -> Exec $ inc j >> return (Grab (go SPEC))
        Nothing           -> Exec $ inject <$> G.unsafeFreeze ar
      {-# INLINE [0] inc #-}
      inc p = M.unsafeRead ar p >>= M.unsafeWrite ar p . succ
  return $ Grab (go SPEC)

tranS :: Tube (Elem a) (Elem a) m r
{-# INLINE [1] tranS #-}
tranS  = fun (\(Elem i j x) -> Elem j i x)

ridxs :: (Storable a, PrimMonad m) =>
         Z -> Z -> Vector Z -> Tube (Elem a) () m (CCSMat a)
{-# INLINE [1] ridxs #-}
ridxs nz rn cc = Exec $ do
  let cn = len cc
  rr <- M.unsafeNew nz
  xr <- M.unsafeNew nz
  cr <- M.unsafeNew (cn+1)
  let mkc !p !c
        | p  <  cn  = let p' = succ p
                      in  M.unsafeWrite cr p' c >> mkc p' (c+cc!p)
        | otherwise = return ()
  M.unsafeWrite cr 0 0 >> mkc 0 0
  let go !_ me = case me of
        Just (Elem i j x) -> Exec $ do
          p <- M.unsafeRead cq j
          M.unsafeWrite cq j (p+1)
          M.unsafeWrite rr p i
          M.unsafeWrite xr p x
          return (Grab (go SPEC))
        Nothing -> Exec $ do
          ((inject .) .) . CCS rn <$> G.unsafeFreeze rr <*>
            G.unsafeFreeze cr <*> G.unsafeFreeze xr
      cq = M.unsafeTail cr
  return (Grab (go SPEC))

fun :: (a -> b) -> Tube a b m r
{-# INLINE [1] fun #-}
fun f = Grab (go SPEC)
  where
    go !_ me = case me of
      Just  x -> Grab (go SPEC) $< f x
      Nothing -> done


-- * Matrix (re-)shape functions.
------------------------------------------------------------------------------
trilP :: Storable a => CCSMat a -> CCSMat a
{-# SPECIALIZE trilP :: CCSD -> CCSD #-}
trilP mm = runST $ do
  let (rn, cn, nz) = (getRowN mm, getColN mm, getNNZ mm)
      {-# INLINE ff #-}
      ff !(Elem i j _) = i >= j
  Just cc <- runE $ elems mm `fuse` filts ff `fuse` ccnts cn
  Just qq <- runE $ elems mm `fuse` filts ff `fuse` ridxs nz rn cc
  return qq

tsrc :: forall a s r. Storable a => CCSMat a -> Src (Elem a) (ST s) r
{-# INLINE [1] tsrc #-}
tsrc mm = elems mm `fuse` tranS

trans :: forall a. Storable a => CCSMat a -> CCSMat a
{-# SPECIALIZE trans :: CCSD -> CCSD #-}
trans mm = runST $ do
  let (rn, cn, nz) = (getRowN mm, getColN mm, getNNZ mm)
--       src = elems mm `fuse` tranS
--   Just cc <- runE $ elems mm `fuse` tranS `fuse` ccnts cn
  Just cc <- runE $ tsrc mm `fuse` ccnts cn
--   Just qq <- runE $ elems mm `fuse` tranS `fuse` ridxs nz rn cc
  Just qq <- runE $ tsrc mm `fuse` ridxs nz rn cc
  return qq


-- * Some stream operations.
------------------------------------------------------------------------------
headS :: a -> Tube a a m r
headS x = Grab (go x)
  where
    go z me = case me of
      Just z' -> final z'
      Nothing -> final z

lastS :: a -> Tube a a m r
{-# INLINE [1] lastS #-}
lastS x = Grab (go SPEC x)
  where
    go !_ z me = case me of
      Nothing -> final z
      Just z' -> Grab (go SPEC z')

head' :: Tube a a m r
head'  = Grab $ final . fromJust

last' :: Tube a a m r
last'  = Grab $ lastS . fromJust


testPipes = do
  let ri = vec [0,0,1]
      cp = vec [0,1,3]
      xs = vec [1,2,3] :: Vector Double
      mm = CCS 2 ri cp xs
      rj = vec [0,1]
      cq = vec [0,0,2]
      ys = vec [4,5] :: Vector Double
      qq = CCS 2 rj cq ys
  print mm
  print =<< runT (elems mm)
  print =<< runT (elems mm `fuse` cptrs)
  print =<< runT (elems qq)
  print =<< runT (elems qq `fuse` cptrs)
  print =<< runT (elems mm `fuse` head')
  print =<< runT (elems mm `fuse` last')
  print =<< runT (elems mm `fuse` (inject 'a' :: Tube ElemD Z IO Char))
  print $ trans mm
  print $ trans qq
