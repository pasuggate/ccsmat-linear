{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, OverloadedStrings,
             FlexibleContexts, BangPatterns, RecordWildCards, TupleSections,
             FlexibleInstances, TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Submatrix
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Build matrices from submatrices, or decompose matrices into submatrices.
--
-- NOTE:
-- 
-- Changelog:
--  + 12/05/2017  --  initial file;
-- 
-- TODO:
--  + the functions here are slow, in barely-tested, so check and improve;
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Submatrix
  ( toBlocksEvery
  , fromBlocks
  ) where

import GHC.Types (SPEC(..))
import Data.List as List
import qualified Data.IntMap as Map
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as S
import Data.Vector.Fusion.Stream.Monadic (Stream(..), Step(..))
import Data.Vector.Fusion.Util (Id(..))

import Data.Vector.Helpers
import Data.CCSMat.Internal
import Data.CCSMat.Stream
import Data.CCSMat.MapMat
import Data.CCSMat.RowCol


-- * Break into submatrices.
------------------------------------------------------------------------------
-- | Fully partition a matrix into blocks of the same size. If the dimensions
--   are divisible by the given sizes, then the last blocks will be smaller.
--   NOTE: Generates output that is row-major (as these are easier to format
--     using pattern-matching).
toBlocksEvery :: (Storable a, Num a) => Z -> Z -> CCSMat a -> [[CCSMat a]]
{-# SPECIALISE toBlocksEvery :: Z -> Z -> CCSD -> [[CCSD]] #-}
toBlocksEvery m n cc =
  let mm = mmMergeWith (flip const) bs . toMapMatMapMat m n . toElems
      mc = unMMF . fmap (fromMapMat m n) . MMF
      bs = blanks (pred k `div` m + 1) (pred l `div` n + 1) Map.empty
      (k, l) = getDims cc
      go = Map.elems . fmap Map.elems . mc . mm
  in  List.transpose $ go cc

toMapMatMapMat :: Num a => Z -> Z -> [Elem a] -> MapMat (MapMat a)
toMapMatMapMat m n =
  fromElemsWith mmMerge . map (fmap mmUnit) . blockElems m n


-- * Compose from submatrices.
------------------------------------------------------------------------------
-- | Assemble from a 2D array of (equal-sized) submatrices.
--   NOTE: Assumes that the input is row-major (as these are easier to format
--     using pattern-matching).
--   
--   TODO: Test inputs for sanity.
fromBlocks :: (Storable a, Num a) => [[CCSMat a]] -> CCSMat a
fromBlocks bs =
  let (m, n)  = getDims (head (head bs))
      (r, s)  = (length bs, length (head bs))
      go i j  = map `flip` toElems mm $ \(Elem k l x) -> Elem (k+p) (l+q) x
        where (p, q, mm) = (i*m, j*n, bs!!i!!j)
--       es = concat [ go i j | j <- [0..s-1], i <- [0..r-1] ]
      es = concat [ go i j | i <- [0..r-1], j <- [0..s-1] ]
  in  elemsToCCS' (r*m) (s*n) es


-- * Block-element constructors.
------------------------------------------------------------------------------
blockElems :: Functor f => Z -> Z -> f (Elem a) -> f (Elem (Elem a))
{-# SPECIALISE blockElems :: Z -> Z -> [Elem R] -> [Elem (Elem R)] #-}
blockElems m n = fmap (go SPEC)
  where go !_ (Elem i j x) =
          let (k, l) = (i `mod` m, j `mod` n)
              (p, q) = (i `div` m, j `div` n)
          in  Elem p q (Elem k l x)

------------------------------------------------------------------------------
-- | Construct an `[m x n]` `MapMat` using the given element for each entry.
blanks :: Z -> Z -> a -> MapMat a
blanks m n x = foldl (mmMergeWith const) Map.empty ms
  where ms = mmUnit <$> [ Elem i j x | i <- [0..m-1], j <- [0..n-1] ]
