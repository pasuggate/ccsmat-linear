{-# LANGUAGE BangPatterns, TupleSections, GADTs, Rank2Types,
             FlexibleContexts, FlexibleInstances, ScopedTypeVariables,
             MultiParamTypeClasses
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Stream
-- Copyright   : (C) Patrick Suggate, 2015
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Stream-fusion functionality, for sparse matrices.
-- 
-- Changelog:
--  + 27/08/2015  --  initial file;
-- 
-- TODO:
--  + monadic streams;
--  + flattening of nested streams?
--  + set-ops; e.g., union, etc.?
--  + `SPEC` specialisations?
--  + look at (the fusion in) Kmett's `sparse` library;
--  + what use-cases allow for SIMD?
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Stream
       ( module Data.CCSMat.Element
       , SpMV (..)
       , runS
       , unstream
       , vstream
       , elemS
       , elemW
         -- Queries:
       , indiceS
       , rowcountS
       , rowcountM
         -- Shape operations:
       , tranS
       , trilS
       , trilF
       , trilF'
       , triuS
       , filtM
       , isLowerM
       , isLowerS
         -- Streams of columns:
       , colS
       , getColS
       , fromColS
       , colIdxS
         -- Algebraic operations:
       , mulS
       , spmv_stream
       , testStream
       ) where

import GHC.Types (SPEC(..))
import Control.Monad.Primitive
import Control.Monad.ST
import Control.Monad.Identity
import Data.Bool
import Data.Vector.Helpers
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Generic.Mutable as M
import Data.Vector.Storable (Vector, Storable)

import Data.Vector.Fusion.Stream.Monadic (Stream(..), Step(..))
import qualified Data.Vector.Fusion.Stream.Monadic as Stream

-- import Data.Vector.Sparse.Stream hiding (filtM)
import Data.Vector.Sparse
-- import qualified Data.Vector.Sparse.Stream as Sparse
import Data.CCSMat.Internal
import Data.CCSMat.Element
import Data.CCSMat.Vector (spmspvM, vspm', sparseCol, SpMV (..))
import Text.Printf


-- * Instances
------------------------------------------------------------------------------
-- | Sparse matrix times (dense) vector.
instance (Num a, Storable a) => SpMV (Vector a) a where
  spmv = spmv_stream SPEC
  vspm = vspm'
  {-# INLINE spmv #-}
  {-# INLINE vspm #-}


-- * Stream evaluation.
------------------------------------------------------------------------------
-- | Evaluate to a list.
runS :: Monad m => Stream m a -> m [a]
runS (Stream step t) = go SPEC t
  where
    go !_ s = step s >>= \r -> case r of
      Yield x s' -> (x:) <$> go SPEC s'
      Skip    s' -> go SPEC s'
      Done       -> return []
{-# INLINE [1] runS #-}

------------------------------------------------------------------------------
-- | Convert the stream to a vector.
unstream :: (PrimMonad m, G.Vector v a) => Z -> Stream m a -> m (v a)
unstream n (Stream step t) = do
  ar <- new n
  let go !_ p s = step s >>= \r -> case r of
        Yield x s' -> wr ar p x >> go SPEC (p+1) s'
        Skip    s' -> go SPEC p s'
        Done       -> frz ar
  go SPEC 0 t
{-# INLINE [1] unstream #-}

------------------------------------------------------------------------------
-- | Take the first `n` elements, from the given stream.
unstreamN ::
     PrimMonad m
  => G.Vector v a
  => Z
  -> Stream m a
  -> m (Maybe (v a, Stream m a))
unstreamN n (Stream step t) = do
  ar <- new n
  let go !_ !p !s
        | p  <  n   = step s >>= \r -> case r of
          Yield x s' -> wr ar p x >> go SPEC (p+1) s'
          Skip    s' -> go SPEC p s'
          Done       -> bool (fin s . tak p <$> frz ar) (return Nothing) (p > 0)
        | otherwise = fin s <$> frz ar
      fin s = Just . (,Stream step s)
      {-# INLINE fin #-}
  go SPEC 0 t
{-# INLINE [1] unstreamN #-}

vstream :: (Monad m, Storable a) => Vector a -> Stream m a
vstream vs = Stream step 0
  where
    n = len vs
    step p | p  <  n   = return $ Yield (vs!p) (p+1)
           | otherwise = return $ Done
    {-# INLINE [0] step #-}
{-# INLINE [1] vstream #-}


-- * Matrix streams.
------------------------------------------------------------------------------
-- | Construct a stream of indices and elements.
elemS :: (Monad m, Storable a) => CCSMat a -> Stream m (Elem a)
elemS  = elemW Elem tt
  where
    tt _ _ = const True
    {-# INLINE [0] tt #-}
{-# INLINE [1] elemS #-}

elemW :: (Monad m, Storable a) =>
         (Z -> Z -> a -> b) -> (Z -> Z -> a -> Bool) -> CCSMat a -> Stream m b
elemW f g (CCS _ ri cp xs) = Stream step (Ix 0 0)
  where
    step !(Ix j p)
      | p  >=  nz = return   Done
      | g i j' x  = return $ Yield (f i j' x) s'
      | otherwise = return $ Skip             s'
      where s' = Ix j' (p+1)
            j' = col j p
            i  = ri!p
            x  = xs!p
    {-# INLINE [0] step #-}
    col !j !p
      | p  ==  q  = col j' p
      | otherwise = j
      where q  = cp!j'
            j' = j+1
    nz = len ri
{-# INLINE [1] elemW #-}

------------------------------------------------------------------------------
-- | Construct a stream of indices.
indiceS :: (Monad m, Storable a) => CCSMat a -> Stream m Ix
indiceS  = elemW go tt
  where
    go i j = const (Ix i j)
    {-# INLINE [0] go #-}
    tt _ _ = const True
    {-# INLINE [0] tt #-}
{-# INLINE [1] indiceS #-}


-- ** Streams-of-columns operations.
------------------------------------------------------------------------------
-- | Decompose a matrix into a stream of columns.
colS :: (Monad m, Storable a) => CCSMat a -> Stream m (SparseVector a)
colS (CCS rn ri cp xs) = Stream step 0
  where
    cn = len cp - 1
    col !p !n = SparseVector rn (slc p n ri) (slc p n xs)
    {-# INLINE [0] col #-}
    step j | j  <  cn  = return $ Yield (col p (cp!j' - p)) j'
           | otherwise = return   Done
      where j' = j+1
            p  = cp!j
    {-# INLINE [0] step #-}
{-# INLINE [1] colS #-}

-- | Extract the columns using the given stream of column indices.
getColS :: (Monad m, Storable a) =>
           CCSMat a -> Stream m Z -> Stream m (SparseVector a)
getColS m = Stream.map (sparseCol m)
{-# INLINE [1] getColS #-}

------------------------------------------------------------------------------
-- | Assemble a sparse-matrix using the given stream of column-vectors.
fromColS :: (PrimMonad m, Storable a) =>
            Z -> Z -> Stream m (SparseVector a) -> m (CCSMat a)
fromColS rn cc ss = do
  (nz, cp) <- countS cc ss
  fillS rn nz cp ss
{-# INLINE [1] fromColS #-}

colIdxS :: Monad m => CCSMat a -> Stream m (Vector Z)
colIdxS (CCS _ ri cp _) = Stream step 0
  where
    cc = len cp
    step j | j' <  cc  = return $ Yield (colIdxs' ri cp j) j'
           | otherwise = return $ Done
      where j' = j+1
    {-# INLINE [0] step #-}
{-# INLINE [1] colIdxS #-}

------------------------------------------------------------------------------
-- | Count the number of elements in each column-vector, and then use these
--   counts to build the column-pointers.
countS ::
     PrimMonad m
  => Storable a
  => Z
  -> Stream m (SparseVector a)
  -> m (Z, Vector Z)
countS cc (Stream step t) = do
  cr <- new cc
  let cnt !_ !j !p s = step s >>= \r -> wr cr j p >> case r of
        Yield v s' -> cnt SPEC j' (p+slen v) s'
        Skip    s' -> cnt SPEC j'  p         s'
        Done       -> (p,) <$> frz cr
        where
          j' = j+1
      slen = len . indices
      {-# INLINE [0] slen #-}
  cnt SPEC 0 0 t
{-# INLINE [1] countS #-}

-- | Using the given vector of column-pointers, fill the row-indices and
--   element-values into a new sparse-matrix.
fillS ::
     PrimMonad m
  => Storable a
  => Z
  -> Z
  -> Vector Z
  -> Stream m (SparseVector a)
  -> m (CCSMat a)
fillS rn nz cp (Stream step t) = do
  rr <- new nz
  xr <- new nz
  let go !_ !j s = step s >>= \r -> case r of
        Yield (SparseVector _ ri xs) s' -> cpy ri xs (cp!j) >> go SPEC j' s'
        Skip                         s' -> go SPEC j' s'
        Done                            -> return ()
        where j' = j+1
      cpy ii xx p = do
        let n = len ii
        vcp (mlc p n rr) ii
        vcp (mlc p n xr) xx
      {-# INLINE [0] cpy #-}
  go SPEC 0 t
  flip (CCS rn) cp <$> frz rr <*> frz xr
{-# INLINE [1] fillS #-}

------------------------------------------------------------------------------
-- | Apply the given filtering-function, to a sparse matrix.
filtM ::
     Storable a
  => PrimMonad m
  => (Z -> Z -> a -> Bool)
  -> CCSMat a
  -> m (CCSMat a)
filtM f mm = do
  let (rn, cn) = (getRowN mm, getColN mm)
      es = elemW Elem f mm
  cc <- ccntS cn es
  ridxS (G.sum cc) rn cc es
{-# INLINE [1] filtM #-}

------------------------------------------------------------------------------
-- | Build a matrix from a stream of `Elem's.
--   
--   NOTE: Without the matrix non-zero count, dimensions, and column- counts/
--     pointers, this algorithm does three passes, which is very slow.
--   NOTE: Doesn't handle duplicate entries.
--   
fromElemS :: (Storable a, PrimMonad m) => Stream m (Elem a) -> m (CCSMat a)
fromElemS (Stream step t) = do
  let dim !_ !r !c !s = step s >>= \z -> case z of
        Yield (Elem i j _) s' -> dim SPEC (max r i) (max c j) s'
        Skip               s' -> dim SPEC r c s'
        Done                  -> return (r+1, c+1)
  (rn, cn) <- dim SPEC 0 0 t
  cx  <- new (cn+2)
  ccs <- const (M.drop 1 cx) <$> wr cx 0 0
  let shp !_ !s = step s >>= \r -> case r of
        Yield (Elem _ j _) s' -> mfy ccs (+1) j >> shp SPEC s'
        Skip               s' -> shp SPEC s'
        Done                  -> return ()
  shp SPEC t
  let cnt !_ !p !j
        | j  <  cn  = rd ccs j >>= \c -> wr ccs j p >> cnt SPEC (p+c) (j+1)
        | otherwise = wr ccs j p >> return p
  nz  <- cnt SPEC 0 0
  rx  <- new nz
  xx  <- new nz
  let upd i j x = rd ccs j >>= \p -> wr ccs j (p+1) >> wr rx p i >> wr xx p x
      {-# INLINE upd #-}
      mat !_ !s = step s >>= \r -> case r of
        Yield (Elem i j x) s' -> upd i j x >> mat SPEC s'
        Skip               s' -> mat SPEC s'
        Done                  -> CCS rn <$> frz rx <*> tak (cn+1) `fmap` frz cx <*> frz xx
  mat SPEC t
{-# SPECIALIZE fromElemS :: Stream IO (Elem R) -> IO (CCSMat R) #-}
{-# INLINABLE  fromElemS #-}

fromElems :: Storable a => [Elem a] -> CCSMat a
fromElems xs = runST $ fromElemS (Stream step xs)
  where
    step (x:xs') = return $ Yield x xs'
    step      _  = return $ Done
{-# SPECIALIZE fromElems :: [Elem R] -> CCSMat R #-}
{-# INLINABLE  fromElems #-}


-- * Stream helper-functions.
------------------------------------------------------------------------------
-- | Count the numbers of elements within each column.
ccntS :: (PrimMonad m, G.Vector v Z) => Z -> Stream m (Elem a) -> m (v Z)
ccntS  = ccntW prj where
  prj !(Elem _ j _) = j
{-# INLINE [1] ccntS #-}

-- | Count the numbers of elements within each column, using the given
--   projection function.
ccntW ::
     forall m a v.
     PrimMonad m
  => G.Vector v Z
  => (a -> Z)
  -> Z
  -> Stream m a
  -> m (v Z)
ccntW f cn (Stream step t) = do
  ar <- thw (rep cn 0 :: v Z)
  let go !_ !s = step s >>= \r -> case r of
        Yield x s' -> inc (f x) >> go SPEC s'
        Skip    s' -> go SPEC s'
        Done       -> frz ar
      inc p = rd ar p >>= wr ar p . (+1)
      {-# INLINE [0] inc #-}
  go SPEC t
{-# INLINE [1] ccntW #-}

------------------------------------------------------------------------------
-- | Given the vector of row-indices, count the number of elements within each
--   row.
--   NOTE: Useful for operations like transposition.
rowcountS :: Vector Z -> Z -> Vector Z
rowcountS ri cn = runST $ rowcountM ri cn
{-# INLINE [1] rowcountS #-}

rowcountM :: PrimMonad m => Vector Z -> Z -> m (Vector Z)
rowcountM ri cn = do
  ar <- thw (rep cn 0 :: Vector Z)
  let go !p | p  <  nz  = inc (ri!p) >> go (p+1)
            | otherwise = frz ar
      nz = len ri
      inc p = rd ar p >>= wr ar p . (+1)
      {-# INLINE [0] inc #-}
  go 0
{-# INLINE [1] rowcountM #-}

------------------------------------------------------------------------------
-- | Given the non-zero count, the number of rows, and the column element-
--   counts, assemble a matrix from the given stream of elements.
--   
--   TODO: Are the `SPEC's needed, and probably not for the `mkc` function?
ridxS ::
     Storable a
  => PrimMonad m
  => Z
  -> Z
  -> Vector Z
  -> Stream m (Elem a)
  -> m (CCSMat a)
ridxS nz rn cc (Stream step t) = do
  let cn = len cc
  rr <- new nz
  xr <- new nz
  cr <- new (cn+1)
  let mkc !_ !p !c
        | p  <  cn  = let p' = p+1
                      in  wr cr p' c >> mkc SPEC p' (c+cc!p)
        | otherwise = return ()
  wr cr 0 0 >> mkc SPEC 0 0
  let go !_ !s = step s >>= \r -> case r of
        Yield (Elem i j x) s' -> do
          p <- rd cq j
          wr cq j (p+1) >> wr rr p i >> wr xr p x >> go SPEC s'
        Skip               s' -> go SPEC s'
        Done                  -> CCS rn <$> frz rr <*> frz cr <*> frz xr
      cq = M.tail cr
  go SPEC t
{-# INLINE [1] ridxS #-}


-- * Matrix reshaping operations.
------------------------------------------------------------------------------
tranS :: Storable a => CCSMat a -> CCSMat a
tranS mm = runST $ tranM mm
{-# INLINE [1] tranS #-}

tranM :: (Storable a, PrimMonad m) => CCSMat a -> m (CCSMat a)
tranM mm@(CCS rn ri _ _) = do
  let sw i j = Elem j i
      tt _ _ = const True
  cc <- rowcountM ri rn
  ridxS (len ri) rn cc (elemW sw tt mm)
{-# INLINE [1] tranM #-}

------------------------------------------------------------------------------
-- | Fast, computation of the lower-triangular matrix, for the given matrix.
--   TODO: Move to `Data.CCSMat.Shape`.
trilF :: Storable a => CCSMat a -> CCSMat a
trilF mm = runST $ trilF' mm
{-# SPECIALIZE trilF :: CCSD -> CCSD #-}

trilF' :: (PrimMonad m, Storable a) => CCSMat a -> m (CCSMat a)
trilF' (CCS rn ri cp xs) = do
  let cc = len cp
  pr <- new (lst cp)
  cr <- thw (rep cc 0 :: Vector Z)
  let cnt !_ !j !q !p !c
        | p   <  q  = bool (wr pr c p >> cnt SPEC j q p' (c+1))
                      (cnt SPEC j q p' c) (ri!p<j)
        | otherwise = return c
        where p'    = p+1
      go !_ j p
        | j'  <  cc = wr cr j p >> cnt SPEC j (cp!j') (cp!j) p >>= go SPEC j'
        | otherwise = wr cr j p >> return p
        where j'    = j+1
  nz <- go SPEC 0 0
  cq <- frz cr
  ps <- tak nz <$> frz pr
  rr <- new nz
  xr <- new nz
  let cpy i p = wr rr i (ri!p) >> wr xr i (xs!p) >> return (i+1)
      {-# INLINE cpy #-}
  _  <- G.foldM' cpy 0 (ps :: Vector Z)
  flip (CCS rn) cq <$> frz rr <*> frz xr
{-# INLINE [1] trilF' #-}

------------------------------------------------------------------------------
trilS :: Storable a => CCSMat a -> CCSMat a
trilS mm = runST $ filtM tr mm where
  tr i j = const (i >= j)
  {-# INLINE [0] tr #-}
{-# INLINE [1] trilS #-}

triuS :: Storable a => CCSMat a -> CCSMat a
triuS mm = runST $ filtM tr mm where
  tr i j = const (i <= j)
  {-# INLINE [0] tr #-}
{-# INLINE [1] triuS #-}


-- * Matrix algebraic operations.
------------------------------------------------------------------------------
mulS :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
mulS aa bb = runST $ mulM aa bb
{-# SPECIALIZE mulS :: CCSD -> CCSD -> CCSD #-}
-- {-# INLINE [1] mulS #-}

mulM ::
     Num a
  => Storable a
  => PrimMonad m
  => CCSMat a
  -> CCSMat a
  -> m (CCSMat a)
-- mulM aa@(CCS rn _ cp _) = fromColS rn cc . Stream.map (spmspv aa) . colS
mulM aa@(CCS rn _ cp _) = fromColS rn cc . Stream.mapM (spmspvM aa) . colS
  where
    cc = len cp -- FIXME: Needs to be `getRowN bb`?
{-# INLINE [1] mulM #-}

------------------------------------------------------------------------------
-- | Sparse matrix times (dense) vector.
spmv_stream ::
     Num a
  => Storable a
  => SPEC
  -> CCSMat a
  -> Vector a
  -> Vector a
spmv_stream !_ matA xvec
  | cn == vn  = runST $ spmvM matA xvec
  | otherwise = error $ msg rn cn vn
  where
    msg = printf "CCS.Stream.spmv: dimensions don't match ([%d x %d] x [%d])"
    (rn, cn, vn) = (getRowN matA, getColN matA, G.length xvec)
{-# INLINABLE  spmv_stream                                 #-}
{-# SPECIALISE spmv_stream :: SPEC -> CCSD -> DVec -> DVec #-}

------------------------------------------------------------------------------
-- | Sparse matrix times (dense) vector.
spmvM ::
     Num a
  => Storable a
  => PrimMonad m
  => CCSMat a
  -> Vector a
  -> m (Vector a)
spmvM matA xvec =
  let ms = colS matA
      vs = vstream xvec
      ws = Stream.zipWith vecScale vs ms
      x0 = getRowN matA `rep` 0
  in  Stream.foldl' (\x y -> vpspv x y) x0 ws
{-# INLINE spmvM #-}

{-- }
-- | Vector times a sparse matrix.
vspmM ::
  (Num a, Storable a, PrimMonad m) => Vector a -> CCSMat a -> m (Vector a)
{-# INLINE vspmM #-}
spmvM xvec matA =
  let ms = colS matA
      vs = vstream xvec
      ws = Stream.zipWith vecScale vs ms
      x0 = getRowN matA `rep` 0
  in  Stream.foldl' (\x y -> vpspv x y) x0 ws
--}


-- * Matrix querying operations.
------------------------------------------------------------------------------
isLowerS :: Storable a => CCSMat a -> Bool
isLowerS  = runIdentity . isLowerM
{-# INLINE [1] isLowerS #-}

isLowerM :: forall a m. (Storable a, Monad m) => CCSMat a -> m Bool
isLowerM  = Stream.null . elemW ff gg where
  ff _ _ = const ()
  gg i j = const (i < j)
  {-# INLINE [0] ff #-}
  {-# INLINE [0] gg #-}
{-# INLINE [1] isLowerM #-}


-- * Testing and examples.
------------------------------------------------------------------------------
testStream :: IO ()
testStream  = do
  let ri = vec [0,0,1]
      cp = vec [0,1,3]
      xs = vec [1,2,3] :: Vector Double
      mm = CCS 2 ri cp xs
      rj = vec [0,1]
      cq = vec [0,0,2]
      ys = vec [4,5] :: Vector Double
      qq = CCS 2 rj cq ys
  print mm
  print =<< runS (elemS mm)
  print =<< runS (elemS qq)
  print (trilS mm)
  print (trilS qq)
  print (tranS mm)
  print (tranS qq)
