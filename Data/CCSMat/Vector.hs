{-# LANGUAGE BangPatterns, Rank2Types, MultiParamTypeClasses,
             FunctionalDependencies, FlexibleInstances, DefaultSignatures
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.BSpline.Uniform.Integration
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Matrix-vector operations involving CCS sparse matrices.
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Vector
       ( SpMV (..)
       , SparseVector (..)
       -- sparse-matrix times sparse-vector:
       , spmspv
       , spmspvM
       -- sparse-vector times sparse-matrix:
       , vxspm
       , vspm'
       , vXspm
       -- , spmxv
       -- more sparse-vector operations:
       , spouter -- ^ sparse outer-product
       , scatter
       , scatterM
       , symScatter
       , symCol
       , sparseCol
       , getcol -- , sparseRow
       , fullCol
       , fullColUsing
         -- Conversions to lists-of-vectors, and back:
       , fromColVecs
       , fromColVecsM
       , toColVecs
       , toSparseCols
       , fromSparseCols
       , withCols
       , withCols'
       ) where

--
--  A sparse matrix with a single column can be used as a sparse vector.
--  This module contains a couple of funtions to help with this.
--

import GHC.Types (SPEC(..))
import GHC.Exts (inline)
import Control.Monad.ST
import Control.Monad.Primitive
import Data.Bool (bool)
import qualified Data.List as L
import Data.Vector.Helpers
import Data.Vector.Storable (Vector, Storable)
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable as V
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Generic.Mutable as M

import Data.CCSMat.Internal as CCS
import Data.CCSMat.Transpose as CCS
import Data.Vector.Sparse as SpV


-- * Matrix-vector function-classes
------------------------------------------------------------------------------
-- | Sparse-matrix times vector.
class SpMV t a | t -> a where
  spmv :: CCSMat a -> t -> t
  vspm :: t -> CCSMat a -> t
  default vspm :: Storable a => t -> CCSMat a -> t
  v `vspm` m = CCS.transpose m `spmv` v


-- * Matrix-vector related instances
------------------------------------------------------------------------------
instance (Num a, Storable a) => SpMV (SparseVector a) a where
  spmv = spmspv
  {-# INLINE spmv #-}


-- * Operations involving sparse matrices and vectors.
------------------------------------------------------------------------------
-- | Sparse matrix times sparse vector.
--   
--   TODO: Shape-checking.
spmspv :: (Num a, Storable a) => CCSMat a -> SparseVector a -> SparseVector a
-- {-# SPECIALIZE spmspv :: CCSMat Double -> SpVD -> SpVD #-}
{-# INLINE [1] spmspv #-}
spmspv m v = runST $ spmspvM m v

-- | The algorithm first computes the number of non-zeros, in the product
--   sparse-vector, and then performs the sparse-matrix times sparse-vector
--   operation.
spmspvM :: (PrimMonad m, Num a, Storable a) =>
           CCSMat a -> SparseVector a -> m (SparseVector a)
{-# INLINE [1] spmspvM #-}
spmspvM (CCS rn ri cp xs) (SparseVector _ ix vs) = do
  flg <- thw (V.replicate rn (-1) :: Vector Int)
  let {-# INLINE [0] goc #-}
      goc nz i = cnt nz (cp!i) (cp!(i+1))
      cnt !nz !q !r
        | q < r = do
          let i = ri!q
          f <- rd flg i
          if f < 0
            then wr flg i nz >> cnt (nz+1) (q+1) r
            else cnt nz (q+1) r
        | otherwise = return nz
  nnz <- G.foldM' goc 0 ix
  ai  <- new nnz
  ax  <- new nnz
  M.set flg (-1)
  let go !_ !nz !p
        | p  >  0   = let p' = p-1
                          j  = ix!p'
                      in  mvm SPEC (vs!p') nz (cp!j) (cp!(j+1)) >>= flip (go SPEC) p'
        | otherwise = return ()
      mvm !_ !y !nz !q !r
        | q < r = do
          let i  = ri!q
              x  = xs!q * y
              q' = q+1
          f <- rd flg i
          if f < 0
            then wr flg i nz >> wr ai nz i >>
                 wr ax nz x >> mvm SPEC y (nz+1) q' r
            else rd ax f >>= wr ax f . (x+) >>
                 mvm SPEC y nz q' r
        | otherwise = return nz
  go SPEC 0 (len ix)
  SparseVector rn <$> frz ai <*> frz ax

------------------------------------------------------------------------------
-- | Outer-product of two sparse vectors.
spouter :: (Storable a, Num a) => SparseVector a -> SparseVector a -> CCSMat a
spouter (SparseVector rn ri xs) (SparseVector cn ci ys) =
  let cc = V.concat [rep nn c | c<-G.toList ci]
      rr = V.concat $ replicate cn ri
      xy = V.concat [G.map (y*) xs | y<-V.toList ys]
      nn = len ri
  in  runST $ sortIntoCols rn cn (nn*len ci) rr cc xy
{-# SPECIALIZE spouter :: SpVD -> SpVD -> CCSD #-}


-- * Sparse row/column extraction.
------------------------------------------------------------------------------
-- | Extract a sparse, column vector, for column `j` of the given matrix.
--   NOTE: Because data isn't modified, just `O(1)` ??
sparseCol :: Storable a => CCSMat a -> Int -> SparseVector a
-- {-# SPECIALIZE sparseCol :: CCSD -> Int -> SpVD #-}
{-# INLINE sparseCol #-}
sparseCol (CCS rn ri cp xs) j =
  let p = cp!j
      n = cp!(j+1) - p
  in  SparseVector rn (slc p n ri) (slc p n xs)

fullCol :: (Num a, Storable a) => CCSMat a -> Int -> Vector a
{-# INLINE [1] fullCol #-}
fullCol  = fullColUsing 0

fullColUsing :: Storable a => a -> CCSMat a -> Int -> Vector a
{-# INLINE [1] fullColUsing #-}
fullColUsing z = (SpV.fullUsing z .) . sparseCol

------------------------------------------------------------------------------
-- | Extract column `j` from the given sparse-matrix, returning a [n x 1]
--   sparse matrix.
getcol :: Storable a => CCSMat a -> Int -> CCSMat a
{-# SPECIALIZE getcol :: CCSD -> Int -> CCSD #-}
getcol m j =
  let SparseVector rn ri xs = sparseCol m j
      cp = vec [0, len ri]
  in  CCS rn ri cp xs


-- * Various functions for building dense vectors, from a sparse matrix.
------------------------------------------------------------------------------
-- | Scatters the non-zero elements of a sparse-vector, into a dense vector,
--   with all other elements being set to zero.
scatter :: (Num a, Storable a) => CCSMat a -> Vector a
{-# SPECIALIZE scatter :: CCSD -> DVec #-}
scatter (CCS rn ri _ xs) = G.update_ z ri xs
  where z = G.replicate rn 0

symScatter :: Storable a => CCSMat a -> Vector Bool
symScatter  = CCS.fullUsing False . xmap (const True)

symCol :: Storable a => CCSMat a -> Int -> Vector Bool
symCol m = SpV.fullUsing False . SpV.sparseMap (const True) . sparseCol m

{-- }
-- | Symbolic scatter, where Booleans are used to represent non-zero elements.
symScatter :: Storable a => CCSMat a -> Vector Bool
{-# SPECIALIZE symScatter :: CCSD -> BVec #-}
symScatter (CCS rn ri _ xs) = G.update_ z ri (G.replicate l True)
  where z = G.replicate rn False
        l = len xs
--}

-- NOTE: `G.unsafeThaw (scatter v)' is faster than this.
scatterM :: (PrimMonad m, Storable a, Num a) =>
           CCSMat a -> m (MVector (PrimState m) a)
{-# INLINE [1] scatterM #-}
scatterM (CCS rn ri cp xs) = do
  xr <- thw $ V.replicate rn 0
  let go !_ i
        | i  <  nz  = wr xr (ri!i) (xs!i) >> go SPEC (i+1)
        | otherwise = return xr
      nz = cp!1
--   let ix = G.enumFromN 0 (cp!1) :: Vector Int
--   G.forM_ ix $ \i -> do
--     wr x' (ri!i) (xs!i)
--   return x'
  go SPEC 0


-- * Construct and decompose sparse matrices, from (dense/full) vectors.
------------------------------------------------------------------------------
-- | Construct a CCS sparse matrix from a list of vectors, and ignoring any
--   zero-elements, within the input vectors.
fromColVecsM :: (PrimMonad m, Storable a, Eq a, Num a) =>
                [Vector a] -> m (CCSMat a)
{-# SPECIALIZE fromColVecsM :: [DVec] -> IO CCSD #-}
fromColVecsM ys = do
  let ys' = G.concat ys
      rn  = len (head ys)
      cn  = length ys
      s   = len ys'
      nnz = getnnz ys' s
      getnnz !xs !p
        | p == 0    = 0
        | x /= 0    = 1 + getnnz xs p'
        | otherwise = getnnz xs p'
          where p' = p-1
                x  = xs!p'
  xs  <- new nnz
  ri  <- new nnz
  ci  <- new nnz
  let fill !p !j
        | j < s     = do
          let j' = j+1
              yj = ys' G.! j
          if yj /= 0
            then wr xs p yj >> wr ri p (j `mod` rn) >>
                 wr ci p (j `div` rn) >> fill (p+1) j'
            else fill p j'
        | otherwise = return ()
  fill 0 0
  ci' <- frz ci
  CCS rn <$> frz ri <*> packColsM cn ci' <*> frz xs

fromColVecs :: (Eq a, Num a, Storable a) => [Vector a] -> CCSMat a
fromColVecs [] = CCS 0 mempty (G.singleton 0) mempty
fromColVecs vs = runST $ fromColVecsM vs

toColVecs :: (Num a, Storable a) => CCSMat a -> [Vector a]
{-# SPECIALIZE toColVecs :: CCSD -> [DVec] #-}
toColVecs   (CCS  0 _ _ _) = []
toColVecs m@(CCS rn _ _ _) = chunk rn $ full m


-- * Conversions to/from sparse vectors.
------------------------------------------------------------------------------
-- | Decompose a sparse matrix into a list of sparse (column) vectors.
toSparseCols :: Storable a => CCSMat a -> [SparseVector a]
toSparseCols m =
  let js = [0..getColN m-1]
  in  map (sparseCol m) js
{-# SPECIALIZE toSparseCols :: CCSD -> [SpVD] #-}
{-# INLINABLE  toSparseCols #-}

-- | Construct a sparse matrix from a list of sparse (column) vectors.
fromSparseCols :: Storable a => [SparseVector a] -> CCSMat a
fromSparseCols [] = CCS 0 mempty (G.singleton 0) mempty
fromSparseCols vs =
  let rn = elemNum (head vs)
      rs = map indices vs
      ri = G.concat rs
      cp = vec . L.scanl' (+) 0 $ map len rs
      xs = G.concat $ map vecData vs
  in  CCS rn ri cp xs
{-# SPECIALIZE fromSparseCols :: [SpVD] -> CCSD #-}


-- * Matrix and vector operations.
------------------------------------------------------------------------------
{-- }
spmxv :: (Storable a, Num a) => CCSMat a -> Vector a -> Vector a
spmxv  = error "CCSMat.Vector.spmxv: unimplemented"
--}

vxspm :: (Storable a, Num a) => Vector a -> CCSMat a -> Vector a
vxspm xs = V.fromList . map (SpV.vspdot xs) . inline toSparseCols
{-# SPECIALIZE   vxspm :: Vector R -> CCSMat R -> Vector R #-}
{-# INLINABLE[2] vxspm #-}

vspm' :: (Num a, Storable a) => Vector a -> CCSMat a -> Vector a
vspm' vs (CCS _ ri cp xs) =
  let col j = let (j', n, p, q) = (j+1, q-p, cp!j, cp!j')
              in  V.sum $ V.zipWith (*) (vs `bkp` slc p n ri) (slc p n xs)
  in  V.map col $ V.enumFromN 0 $ len cp - 1
{-# SPECIALIZE   vspm' :: Vector R -> CCSMat R -> Vector R #-}
{-# INLINABLE[2] vspm' #-}

vXspm :: (Num a, Storable a) => Vector a -> CCSMat a -> Vector a
vXspm vs (CCS _ ri cp xs)
  | len vs ==  0 = vs
  | otherwise    = runST $ do
      let cn = len cp - 1
      ar <- new cn
      let go j p q !x
            | p  ==  q  = wr ar j x >> bool (frz ar) (go j' p q' 0) (j' < cn)
            | otherwise = go j p' q $ x + vs!(ri!p)*xs!p
            where
              (j', p', q') = (j+1, p+1, cp!(j+2))
      go 0 0 (cp!1) 0
{-# SPECIALIZE vXspm :: Vector R -> CCSMat R -> Vector R #-}

{--}
-- TODO:
-- vmsp :: (Num a, Storable a) => Vector a -> CCSMat a -> Vector a
-- vmsp vs (CCS rn ri cp xs) = V.ifoldl' (\x p i -> x + vs!i * xs!p) 0 ri
--}


-- * Helper functions
------------------------------------------------------------------------------
withCols :: Storable a =>
            (Z -> Vector Z -> Vector a -> b) -> CCSMat a -> [b]
withCols f (CCS _ ri cp xs) = go 0 0 where
  go !j !p | j'<len cp = let (q, n) = (cp!j', q-p)
                         in  f j (slc p n ri) (slc p n xs):go j' q
           | otherwise = []
    where j' = j+1
{-# INLINE[2] withCols #-}

withCols' :: Storable a =>
             (Z -> Vector Z -> Vector a -> b) -> CCSMat a -> [b]
withCols' f (CCS _ ri cp xs) = go SPEC 0 0 where
  go !_ !j !p | j'<len cp = let (q, n) = (cp!j', q-p)
                                x = f j (slc p n ri) (slc p n xs)
                            in  x `seq` (x:go SPEC j' q)
              | otherwise = []
    where j' = j+1
{-# INLINE[2] withCols' #-}
