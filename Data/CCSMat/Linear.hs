{-# LANGUAGE BangPatterns #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Linear
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Adds support for some CCS operation on `Linear` data types
-- 
-- Changelog:
--  + 24/11/2018  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Linear
       ( lvxspm
       ) where

import GHC.Types (SPEC(..))
import Data.Vector.Helpers
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import Linear
import Text.Printf

import Data.CCSMat.Internal as CCS
import qualified Data.CCSMat.Vector as CCS


-- * Linear vector & matrix operations
------------------------------------------------------------------------------
-- | Compute the product of a vector and a sparse matrix, but for the case
--   when the vector "coefficients" are not scalars, but some higher-
--   dimensional vector space.
--   
--   NOTE: the operation applied to the nested vector-space coefficients is
--     just scaling.
--   
--   TODO: add specialisations for `M22`, `M23`, ... ?
lvxspm :: (Num a, Storable a, Storable (f a), Additive f) =>
          Vector (f a) -> CCSMat a -> Vector (f a)
lvxspm vs m@(CCS rn _ _ _)
  | len vs == rn = Vec.fromList $ CCS.withCols' go m
  | len vs ==  0 = vs
  | otherwise    = error msg
  where
    msg = printf "vxspm: invalid vector (%d) and matrix (rows: %d) dimensions" (len vs) rn
    go _ ri xs = bkp vs ri `dot` xs
    dot = (Vec.foldl' (^+^) zero .) . Vec.zipWith (^*)
    {-# INLINE dot #-}
{-# INLINABLE  lvxspm #-}
{-# SPECIALIZE lvxspm :: Vector (V1 R) -> CCSMat R -> Vector (V1 R) #-}
{-# SPECIALIZE lvxspm :: Vector (V2 R) -> CCSMat R -> Vector (V2 R) #-}
{-# SPECIALIZE lvxspm :: Vector (V3 R) -> CCSMat R -> Vector (V3 R) #-}
{-# SPECIALIZE lvxspm :: Vector (V4 R) -> CCSMat R -> Vector (V4 R) #-}
