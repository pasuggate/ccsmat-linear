{-# LANGUAGE BangPatterns, TupleSections, FlexibleContexts,
             MultiParamTypeClasses, FlexibleInstances
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Submatrix
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Build matrices from submatrices, or decompose matrices into submatrices.
--
-- NOTE:
-- 
-- Changelog:
--  + 22/09/2015  --  initial file;
--  + 11/05/2017  --  added `mm<*>` operations, and `MMF(..)`;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.CCSMat.MapMat
       ( MapMat
       , MMF(..)
--        , SpMV (..)
       -- conversions and constructors:
       , fromMapMat
       , toMapMat
       , fromElems
       , fromElemsWith
       , elemsToCCS
       , elemsToCCS'
       -- queries:
       , maxCol
       , maxRow
       -- mutliplications:
       , mulMap
       , mulColMap
       , mulMap'
       , mulColMap'
         -- low-level element-wise operations:
       , mmUnit
       , mmInsert
       , mmMerge
       , mmMergeWith
       ) where

------------------------------------------------------------------------------
--
--  Matrices stored as maps of maps (from columns to rows, to elements).
--
--  Changelog:
--   + 22/09/2015  --  initial file;
--

import qualified Data.List as List
import Data.Maybe
-- import Data.IntMap (IntMap)
-- import qualified Data.IntMap as Map
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as Map
import Data.Vector.Helpers
import Data.Vector.Storable(Storable)
import qualified Data.Vector.Generic as G

import Data.CCSMat.Internal
import Data.CCSMat.Element
-- import Data.CCSMat.Stream (Elem(..), ElemD)
import Data.CCSMat.Vector
import Data.Vector.Sparse


-- * `MapMat` data-type.
------------------------------------------------------------------------------
-- | A sparse-matrix representation that uses an `IntMap` of `IntMap's,
--   mapping columns -> rows -> elements.
--   NOTE: This representation doesn't explicitly contain the number of rows
--     and columns.
--   
--   TODO: This should be a `newtype`, so that I can define a bunch of
--     standard instances?
type MapMat a = IntMap (IntMap a)

newtype MMF a = MMF { unMMF :: MapMat a }
              deriving (Eq, Show)


-- * Intances for stuff.
------------------------------------------------------------------------------
instance Functor MMF where
  fmap f = MMF . fmap (fmap f) . unMMF

instance Elemental MMF a where
  toElems = concat . map go . Map.toList . fmap Map.toList . unMMF
    where go (i, jx) = uncurry (Elem i) <$> jx


-- * Builders and converters.
------------------------------------------------------------------------------
toMapMat :: Storable a => CCSMat a -> MapMat a
{-# SPECIALISE toMapMat :: CCSD -> MapMat Double #-}
toMapMat m@(CCS _ _ cp _) =
  let cn = len cp - 2
      js = [0..cn]
      vs = map vec2map $ toSparseCols m
  in  Map.fromList (js `zip` vs)

fromMapMat :: Storable a => Int -> Int -> MapMat a -> CCSMat a
{-# SPECIALISE fromMapMat :: Int -> Int -> MapMat Double -> CCSD #-}
fromMapMat rn cn mm =
  let mkcp n = G.scanl (+) 0 . (G.replicate n 0 G.//) . Map.toList . Map.map Map.size
      mkri = G.concat . Map.elems . Map.map (G.fromList . Map.keys)
      mkxs = G.concat . Map.elems . Map.map (G.fromList . Map.elems)
  in  CCS rn (mkri mm) (mkcp cn mm) (mkxs mm)

elemsToCCS :: (Num a, Storable a) => [Elem a] -> CCSMat a
{-# SPECIALISE elemsToCCS :: [ElemD] -> CCSD #-}
{-# INLINABLE  elemsToCCS #-}
elemsToCCS es =
  let mm = fromElems es
      rn = maxRow mm
      cn = maxCol mm
  in  fromMapMat rn cn mm

elemsToCCS' :: (Num a, Storable a) => Int -> Int -> [Elem a] -> CCSMat a
{-# INLINE elemsToCCS' #-}
elemsToCCS' rn cn = fromMapMat rn cn . fromElems

fromElems :: Num a => [Elem a] -> MapMat a
{-# SPECIALISE fromElems :: [ElemD] -> MapMat Double #-}
{-# INLINABLE  fromElems #-}
fromElems  = flip go mempty
  where
    {-# INLINE upd #-}
    upd (Elem i j x) mm =
      let mj  = fromMaybe mempty $ Map.lookup j mm
          mj' = Map.insertWith (+) i x mj
      in  Map.insert j mj' mm
    go (e:es) = go es . upd e
    go     _  = id

fromElemsWith :: (a -> a -> a) -> [Elem a] -> MapMat a
fromElemsWith f = flip go mempty
  where
    {-# INLINE upd #-}
    upd (Elem i j x) mm =
      let mj  = fromMaybe mempty $ Map.lookup j mm
          mj' = Map.insertWith f i x mj
      in  Map.insert j mj' mm
    go (e:es) = go es . upd e
    go     _  = id


-- * Some queries.
------------------------------------------------------------------------------
maxRow :: MapMat a -> Int
{-# INLINE maxRow #-}
maxRow  = Map.foldl' max 0 . Map.map maxCol

maxCol :: IntMap a -> Int
{-# INLINE maxCol #-}
maxCol  = (+1) . fst . Map.findMax


-- * Mathematical operations on `MapMat's.
------------------------------------------------------------------------------
-- | Multiplies two matrices, using the algorithm:
--     C(:,j) += A(:,k) * B(k,j) ,   j in [0,n-1]
--   NOTE: This is the best of the three `mul's, it produces sorted output,
--     but is much slower than `multiply`.
--   NOTE: Since it uses `IntMap's, it should be fairly fast with very sparse
--     matrices.
--   
--   TODO: Should be easy to build a parallel version of this?
mulMap :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
{-# INLINE mulMap #-}
mulMap x = fromSparseCols . map (mulColMap (toMapMat x)) . toSparseCols

mulColMap :: (Num a, Storable a) => MapMat a -> SparseVector a -> SparseVector a
{-# SPECIALISE mulColMap :: MapMat Double -> SpVD -> SpVD #-}
mulColMap mm (SparseVector n ix vs) =
  let ps = [0..len ix - 1]
      sax p = (vs!p) `vmscale` (mm Map.! (ix!p))
  in  map2vec n $ List.foldl' (\v -> vmadd v . sax) mempty ps

mulMap' :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
{-# INLINE mulMap' #-}
mulMap' x = fromMapMat rn cn . Map.map (mulColMap' (toMapMat x)) . toMapMat
  where (rn, cn) = (getRowN x, getColN x)

mulColMap' :: Num a => MapMat a -> IntMap a -> IntMap a
{-# SPECIALISE mulColMap' :: MapMat Double -> IntMap Double -> IntMap Double #-}
mulColMap' mm vm =
  let sax k x = x `vmscale` (mm Map.! k)
  in  Map.foldlWithKey' (\v k -> vmadd v . sax k) mempty vm


-- * Element-wise operations on `MapMat's.
------------------------------------------------------------------------------
mmInsert :: Int -> Int -> a -> MapMat a -> MapMat a
mmInsert i j x = mmiwu i (Map.singleton j x)

mmUnit :: Elem a -> MapMat a
mmUnit (Elem i j x) = Map.singleton i (Map.singleton j x)

mmMergeWith :: (a -> a -> a) -> MapMat a -> MapMat a -> MapMat a
mmMergeWith  = Map.unionWith . Map.unionWith

mmMerge :: Num a => MapMat a -> MapMat a -> MapMat a
{-# SPECIALISE mmMerge :: MapMat Double -> MapMat Double -> MapMat Double #-}
mmMerge  = mmMergeWith (+)


-- * Helper functions.
------------------------------------------------------------------------------
-- | MapMap Insert With Union (MMIWU).
mmiwu :: Int -> IntMap a -> MapMat a -> MapMat a
{-# INLINE[1] mmiwu #-}
mmiwu  = Map.insertWith Map.union
