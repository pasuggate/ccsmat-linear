{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, TupleSections,
             FlexibleContexts, BangPatterns, RecordWildCards, OverloadedStrings,
             MultiParamTypeClasses, FunctionalDependencies,
             FlexibleInstances, TypeSynonymInstances, InstanceSigs,
             GeneralizedNewtypeDeriving, StandaloneDeriving,
             DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric,
             GADTs, KindSignatures, DataKinds, PolyKinds,
             TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances,
             CPP, TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Assemble
-- Copyright   : (C) Patrick Suggate 2016
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Miscellaneous vector linear-algebra functions.
--
-- NOTE:
-- 
-- Changelog:
--  + 18/09/2016  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Assemble
       ( module Data.Vector.Linear
       , Assemblable (..)
       , toElems
       , stripe
       , compress
       , catRows
       , catCols
       ) where

-- import Prelude hiding (id, (.), const, fst, snd)
-- import GHC.Types (SPEC(..))
-- import GHC.Generics (Generic)
-- import GHC.TypeLits
-- import Control.Category.Prelude
import Control.Arrow (Arrow, arr, (<<<), (>>>), (***), second)
-- import Control.Monad
import Control.Monad.ST
import Control.Monad.Primitive
import Control.Lens ((^.)) -- , makeLenses, (.~), (%~))
import Data.Foldable
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import Data.Vector.Fusion.Stream.Monadic (Stream(..), Step(..))
import Linear

import Data.Vector.Helpers as Help
import Data.Vector.Linear
import Data.Vector.Sparse (SparseVector)
import Data.CCSMat (CCSMat(..))
import qualified Data.CCSMat as CCS
import qualified Data.CCSMat.Vector as CCS
import qualified Data.CCSMat.Stream as CCS
import qualified Data.CCSMat.MapMat as CCS -- (elemsToCCS)

-- HMatrix library imports:
import Numeric.LinearAlgebra (Matrix, Element)
import qualified Numeric.LinearAlgebra as H


-- * System matrix assembly.
------------------------------------------------------------------------------
-- | Assembles a sparse system of linear equations, for the given set of
--   submatrices.
class Storable a => Assemblable a where
  assemble :: Foldable f => f (Vector Z, Vector a) -> CCSMat R

instance Assemblable R where
  {-# INLINE assemble #-}
  assemble  = makeCCS . toList

instance Assemblable (V1 R) where
  {-# INLINE assemble #-}
  assemble  = makeCCS . (second unv1 <$>) . toList

instance Assemblable (M22 R) where
  assemble  = makeCCS . fmap (reindex 2 *** Vec.unsafeCast <<< go) . toList
    where
      go (js, xs) = (js, stripe (len js) xs)
--       go (js, xs) = (js, stripe (len js) $ Vec.map transpose xs)
--   assemble  = makeCCS . fmap (reindex 2 *** Vec.unsafeCast . Vec.map transpose) . toList

instance Assemblable (M33 R) where
  assemble  = makeCCS . fmap (reindex 3 *** Vec.unsafeCast) . toList

instance Assemblable (M44 R) where
  assemble  = makeCCS . fmap (reindex 4 *** Vec.unsafeCast) . toList


-- * Non-square matrix-operator constructors.
------------------------------------------------------------------------------
-- | Builds a [n x 2n] matrix.
instance Assemblable (V2 R) where
  assemble  = makeRectCCS . (arc go <$>) . toList
    where
      go js xs = (js, reindex 2 js, Vec.unsafeCast xs)

-- | Builds a [n x 3n] matrix.
instance Assemblable (V3 R) where
  assemble  = makeRectCCS . (arc go <$>) . toList
    where
      go js xs = (js, reindex 3 js, Vec.unsafeCast xs)

-- | Builds a [n x 4n] matrix.
instance Assemblable (V4 R) where
  assemble  = makeRectCCS . (arc go <$>) . toList
    where
      go js xs = (js, reindex 4 js, Vec.unsafeCast xs)

------------------------------------------------------------------------------
-- | Builds an [n x n] matrix.
instance Assemblable (V1 (V1 R)) where
  assemble  = assemble . map (second unv1) . toList

-- | Builds a [2n x n] matrix.
instance Assemblable (V1 (V2 R)) where
  assemble  = makeRectCCS . (arc go <$>) . toList
    where
      go js xs = (reindex 2 js, js, Vec.unsafeCast xs)

-- | Builds a [3n x n] matrix.
instance Assemblable (V1 (V3 R)) where
  assemble  = makeRectCCS . (arc go <$>) . toList
    where
      go js xs = (reindex 3 js, js, Vec.unsafeCast xs)

-- | Builds a [4n x n] matrix.
instance Assemblable (V1 (V4 R)) where
  assemble  = makeRectCCS . (arc go <$>) . toList
    where
      go js xs = (reindex 4 js, js, Vec.unsafeCast xs)

------------------------------------------------------------------------------
-- | Builds a [n x 2n] matrix.
instance Assemblable (V2 (V1 R)) where
  {-# INLINE assemble #-}
  assemble  = makeRectCCS . (arc go <$>) . toList
--   assemble  = CCS.unsafeTranspose . makeRectCCS . (arc go <$>) . toList
    where
      go js xs = (js, reindex 2 js, Vec.unsafeCast (len js `stripe` xs))
--       go js xs = (reindex 2 js, js, Vec.unsafeCast ((len js `div` 2) `stripe` xs))


-- * Internal helper functions.
------------------------------------------------------------------------------
-- | Convert a symmetric (sub)matrix to a list of elements.
--   NOTE: The input `js` is the vector of the row/column indices.
toElems :: Element a => Vector Z -> Matrix a -> [CCS.Elem a]
{-# SPECIALIZE toElems :: Vector Z -> Matrix R -> [CCS.Elem R] #-}
toElems js = zipWith go ij . Vec.toList . H.flatten
  where
    ij = Vec.toList $ otWith V2 js js
    go (V2 i j) x = CCS.Elem i j x

-- | Construct elements from non-square submatrices.
toElemsRect :: (Storable a, Num a) =>
               Vector Z -> Vector Z -> Vector a -> [CCS.Elem a]
{-# SPECIALIZE toElemsRect :: Vector Z -> Vector Z -> Vector R -> [CCS.Elem R] #-}
toElemsRect js ks = zipWith go jk . Vec.toList
  where
    jk = Vec.toList $ otWith V2 js ks
    go (V2 j k) x = CCS.Elem j k x

------------------------------------------------------------------------------
-- | Using the given list of vectors of indices, and submatrices, construct a
--   Compressed Column Sparse (CCS) matrix.
--   NOTE: Assumes that the vector of elements corresponds to a square
--     submatrix.
makeCCS :: [(Vector Z, Vector R)] -> CCSMat R
-- makeCCS :: Foldable f => f (Vector Z, Vector R) -> CCSMat R
-- makeCCS  = concatMap (mtx >>> arc toElems) >>> CCS.elemsToCCS
makeCCS  = concatMap (mtx >>> arc toElems) >>> CCS.elemsToCCS >>> CCS.unsafeTranspose
  where
    mtx (js, xs) = (js, H.reshape (len js) xs)

makeRectCCS :: [(Vector Z, Vector Z, Vector R)] -> CCSMat R
makeRectCCS  =
--   concatMap (\(js, ks, xs) -> toElemsRect js ks xs) >>> CCS.elemsToCCS
  concatMap (\(js, ks, xs) -> toElemsRect ks js xs) >>> CCS.elemsToCCS >>> CCS.unsafeTranspose

------------------------------------------------------------------------------
-- | Assumes that each index represents a `[m x m]` submatrix element.
reindex :: Z -> Vector Z -> Vector Z
reindex m js = Vec.generate (len js * m) go
  where
    go p = js!div p m * m + mod p m

{-- }
------------------------------------------------------------------------------
-- | Convert a scalar-field operator to a vector-field operator.
--   
--   TODO: useful? does it work?
--   
stretch :: Storable a => Z -> CCSMat a -> CCSMat a
stretch n (CCS rn ri cp xs) = runST $ do
  let nr = rn*n
      nz = len xs*n
      cc = Vec.zipWith (-) (tal cp) cp
      cf p = cc!div p n
      cq = Vec.scanl' (+) 0 $ Vec.generate (len cp*n-n) cf
  rr <- new nz
  xr <- new nz
  let dup c s p q | c  <  n   = wr rr q (ri!p*n+c) >> wr xr q (xs!p) >> dup (c+1) s p (q+s)
                  | otherwise = return ()
      col j p q | p < cp!(j+1) = dup 0 (cc!j) p q >> col j (p+1) (q+1)
                | otherwise    = return ()
      go j | j < len cp-1 = col j (cp!j) (cq!(j*n)) >> go (j+1)
           | otherwise    = return ()
  go 0
  CCS nr <$> frz rr <*> pure cq <*> frz xr
--}

------------------------------------------------------------------------------
-- | Add every two columns together.
compress :: (PrimMonad m, Storable a, Num a) => Z -> CCSMat a -> m (CCSMat a)
compress n mat =
  let sc = squishS n $ CCS.colS $ CCS.unsafeTranspose mat
      cc = CCS.getRowN mat `div` n + 1
  in  CCS.unsafeTranspose <$> CCS.fromColS (CCS.getColN mat) cc sc

squishS :: (PrimMonad m, Storable a, Num a) =>
           Z -> Stream m (SparseVector a) -> Stream m (SparseVector a)
squishS n (Stream step t) = Stream go0 t
  where
    go0 !s = step s >>= \r -> case r of
      Yield v s' -> go1 (n-1) v s' >>= \rr -> case rr of
        Yield w s'' -> return $ Yield w s''
        Done        -> return $ Done
        Skip    s'' -> go1 (n-1) v s''
      Skip    s' -> go0 s'
      Done       -> return $ Done
    go1 !c !x !s
      | c  ==  0  = return $ Yield x s
      | otherwise = step s >>= \r -> case r of
        Yield v s' -> go1 (c-1) (x+v) s'
        Done       -> return $ Done
        Skip    s' -> go1 c x s'

------------------------------------------------------------------------------
-- | Construct a sparse matrix of just zeroes.
zeros :: Storable a => Z -> Z -> CCSMat a
zeros r c = CCS r Vec.empty (succ c `rep` 0) Vec.empty

catRows :: (Storable a, Num a) => CCSMat a -> CCSMat a -> CCSMat a
catRows a b = CCS.unsafeTranspose c
  where
    c = CCS.unsafeTranspose a `catCols` CCS.unsafeTranspose b

catCols :: (Storable a, Num a) => CCSMat a -> CCSMat a -> CCSMat a
catCols a b = runST $ CCS.colS a `catColS` CCS.colS b

catColS :: (PrimMonad m, Storable a, Num a) =>
           Stream m (SparseVector a) -> Stream m (SparseVector a) ->
           m (CCSMat a)
catColS sx sy = do
  xs <- CCS.runS sx
  ys <- CCS.runS sy
  return $ CCS.fromSparseCols $ xs ++ ys


-- * Vector operators.
------------------------------------------------------------------------------
-- | Convert vectors of functors, with stride `w`, to a row-striped vectors.
stripe :: Storable a => Z -> Vector (V2 a) -> Vector a
{-# INLINE stripe #-}
stripe w =
  let go vx | Vec.null vx = []
            | otherwise   = tak w vx:go (drp w vx)
      st (x:xs) = Vec.map (^._x) x:Vec.map (^._y) x:st xs
      st    []  = []
  in  Vec.concat . st . go

{-- }
stripe w =
  let st x = Vec.map (^._x) x Vec.++ Vec.map (^._y) x
  in  st

mingle :: (PrimMonad m, Foldable f, Storable a, Num a) => f (CCSMat a) -> m (CCSMat a)
mingle mats =
  let sc = mergeS n $ CCS.colS $ CCS.unsafeTranspose mat
      cc = CCS.getRowN mat `div` n + 1
  in  CCS.unsafeTranspose <$> CCS.fromColS (CCS.getColN mat) cc sc
--}


-- * Helper functions
------------------------------------------------------------------------------
-- | Very commonly used, so justifies its short (and confusing) name?
arc  :: Arrow hom => (a -> b -> c) -> (a, b) `hom` c
arc   = arr . uncurry
