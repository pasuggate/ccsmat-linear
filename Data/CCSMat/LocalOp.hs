{-# LANGUAGE DeriveGeneric, TupleSections, OverloadedLists, InstanceSigs,
             ScopedTypeVariables, TemplateHaskell, TypeOperators, BangPatterns
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.LocalOp
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Local operators, that gather/scatter contributions from/to global node
-- coefficients.
-- 
-- Changelog:
--  + 31/05/2019  --  initial file;
--  + 09/02/2020  --  refactored into its own module;
-- 
------------------------------------------------------------------------------

module Data.CCSMat.LocalOp
  ( -- data types & lenses:
    LocalOp (..)
  , locIdx
  , locMat
  -- affine functions and operators:
  , AffineOp (..)
  , getAffineOp
  , affineOp
  , affineOp'
  , linearise
  , yamlAffineOp
  ) where

import GHC.Generics (Generic)
import Control.Monad.IO.Class
import Control.Monad.ST
import Control.Lens (makeLenses)
import Control.DeepSeq
-- import Data.VectorSpace
import Data.Vector.Helpers
import Data.Vector.Storable (Vector)
import qualified Data.ByteString as BS
import qualified Data.Yaml.Pretty as Y
import Data.Aeson

import Data.Vector.Sparse
import Data.CCSMat (CCSMat (..))
import qualified Data.CCSMat as CCS


-- * Data types for local operators
------------------------------------------------------------------------------
-- | Used to gather/scatter the global/local DoF-nodes/contributions, for the
--   listed DoF ID's.
--   
--   NOTE: the operator is stored in "scatter" form -- which is just the
--     (matrix-) transposed version of the "gather" form.
--   
data LocalOp =
  LocalOp { _locIdx :: !(Vector Z), _locMat :: !(CCSMat R) }
  deriving (Eq, Show, Generic)

------------------------------------------------------------------------------
-- | Specialises the @LocalOp@ data-type to support just affine
--   transformations.
newtype AffineOp = AffineOp { _getAffineOp :: LocalOp }
  deriving (Eq, Show, Generic)


-- * Lenses, Prisms, and instances for the data structures
------------------------------------------------------------------------------
makeLenses ''LocalOp
makeLenses ''AffineOp

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData LocalOp
instance NFData AffineOp

-- ** (De)serialisation instances
------------------------------------------------------------------------------
instance ToJSON   LocalOp
instance FromJSON LocalOp

instance ToJSON   AffineOp
instance FromJSON AffineOp


-- * Constructors
------------------------------------------------------------------------------
-- | Construct an affine operator.
--   
--   NOTE: assumes that the operator is stored in "scatter" form -- which is
--     just the (matrix-) transposed version of the "gather" form.
--   
affineOp :: Vector Z -> CCSMat R -> AffineOp
affineOp js = AffineOp . LocalOp js
{-# INLINE affineOp #-}

------------------------------------------------------------------------------
-- | Construct an affine operator, from a "gather" local operator.
--   
--   NOTE: assumes that the operator is stored in "gather" form -- which is
--     just the (matrix-) transposed version of the "scatter" form.
--   
affineOp' :: Vector Z -> CCSMat R -> AffineOp
affineOp' js = affineOp js . CCS.unsafeTranspose
{-# INLINE affineOp' #-}

------------------------------------------------------------------------------
-- | Split out the last column of the affine operator, yielding a linear
--   operator and a (sparse) vector of constant-offsets.
--
linearise :: AffineOp -> (LocalOp, SpV R)
linearise (AffineOp (LocalOp ix (CCS rn ri cp xs))) = runST $ do
  let cn  = len cp - 1
      r'  = rn - 1     -- index of the last row, in each column

  -- Count the nnz in the last row of the matrix:
  let spc :: Int -> Int -> Int -> Int
      spc !j !p !c
        | j >= cn                 = c
        | q > p && ri!(q-1) == r' = spc j' q c'
        | otherwise               = spc j' q c
        where
          (j', c', q) = (j+1, c+1, cp!j')
      nz  = spc 0 0 0

  -- Construct the sparse-vector from the last row of the (transposed)
  -- affine operator:
  vai <- new nz
  vax <- new nz
  let svf !i !j !p
        | j >= cn        = SpV cn <$> frz vai <*> frz vax
        | q > p && r==r' = wr vai i j >> wr vax i (xs!(q-1)) >> svf i' j' q
        | otherwise      = svf i j' q
        where
          (i', j', q, r) = (i+1, j+1, cp!j', ri!(q-1))
  off <- svf 0 0 0

  -- Trim the last row from the (transposed) affine operator matrix:
  mai <- new (lst cp - nz)
  maq <- new (len cp)
  maz <- new (lst cp - nz)
  let smf !j !o !p !q
        | p >= lst cp = wr maq j' o >>
                        CCS r' <$> frz mai <*> frz maq <*> frz maz
        | p    == q   = wr maq j' o >> smf j' o q q'
        | ri!p == r'  = smf j o p' q
        | otherwise   = wr mai o (ri!p) >> wr maz o (xs!p) >> smf j o' p' q
        where
          (o', p', q', j') = (o+1, p+1, cp!(j+2), j+1)
  op' <- smf (-1) 0 0 0

  pure (LocalOp ix op', off)


-- * Import/export
------------------------------------------------------------------------------
-- | Export an affine-operator to a YAML file.
yamlAffineOp ::
     MonadIO m
  => FilePath
  -> AffineOp
  -> m ()
yamlAffineOp fp =
  let ycfg = Y.setConfCompare compare Y.defConfig
  in  liftIO . BS.writeFile fp . Y.encodePretty ycfg
