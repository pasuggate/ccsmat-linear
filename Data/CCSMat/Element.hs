{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, TupleSections,
             FlexibleContexts, BangPatterns, RecordWildCards, OverloadedStrings,
             MultiParamTypeClasses, FunctionalDependencies,
             FlexibleInstances, TypeSynonymInstances, InstanceSigs,
             GeneralizedNewtypeDeriving, StandaloneDeriving,
             DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric,
             GADTs, KindSignatures, DataKinds, PolyKinds,
             TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances,
             CPP, TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Element
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Build matrices from submatrices, or decompose matrices into submatrices.
--
-- NOTE:
-- 
-- Changelog:
--  + 12/05/2017  -- initial file;
-- 
-- TODO:
--  + stewpids/pointless (as a separate module, and its class)?
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Element where

import GHC.Generics (Generic)
-- import GHC.Types (SPEC(..))
import Control.DeepSeq
import Data.Vector.Helpers
import Data.Vector.Storable (Storable)
import qualified Data.Vector.Storable as S
import Data.Vector.Fusion.Stream.Monadic (Stream(..), Step(..))
import Data.Vector.Fusion.Util (Id(..))

import Data.CCSMat.Internal


-- * Elemental functionality.
------------------------------------------------------------------------------
class Elemental c a where
  toElems :: c a -> [Elem a]


-- * Types useful for streaming.
------------------------------------------------------------------------------
-- | Matrix elements.
--   NOTE: Ordering is:
--     Elem i j x
data Elem a =
  Elem {-# UNPACK #-} !Z {-# UNPACK #-} !Z !a
  deriving (Eq, Show, Generic)

-- data Elem a = Elem {-# UNPACK #-} !Z {-# UNPACK #-} !Z a
-- data Elem a = Elem !Z !Z !a
-- data Elem a = Elem Z Z a

type ElemD  = Elem Double

data Ix = Ix {-# UNPACK #-} !Z {-# UNPACK #-} !Z
-- data Ix = Ix !Z !Z
-- data Ix = Ix Z Z


-- * Some instances.
------------------------------------------------------------------------------
instance Functor Elem where
  fmap f (Elem i j x) = Elem i j (f x)
  {-# INLINE fmap #-}

instance Storable a => Elemental CCSMat a where
  toElems = toElems'
  {-# INLINE[2] toElems #-}

-- ** Strictness instances
------------------------------------------------------------------------------
instance NFData a => NFData (Elem a)


-- * Element helper-functions.
------------------------------------------------------------------------------
toElems' :: Storable a => CCSMat a -> [Elem a]
toElems' (CCS _ ri cp xs) =
  let ci = p2j cp
  in  zipWith3 Elem (S.toList ri) (S.toList ci) (S.toList xs)
{-# SPECIALISE toElems' :: CCSD -> [ElemD] #-}

toElemS :: (Monad m, Storable a) => CCSMat a -> Stream m (Elem a)
toElemS (CCS _ ri cp xs) = Stream step 0
  where step i | i < len xs = pure $ Yield (Elem (ri!i) (ci!i) (xs!i)) (i+1)
               | otherwise  = pure $ Done
        ci = p2j cp
{-# SPECIALISE toElemS :: CCSD -> Stream Id ElemD #-}
