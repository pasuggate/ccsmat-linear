{-# LANGUAGE BangPatterns, DataKinds, PolyKinds, TypeFamilies, InstanceSigs,
             GADTs, FlexibleContexts, FlexibleInstances, OverloadedStrings,
             ViewPatterns, PatternSynonyms, TupleSections, OverloadedLists,
             DeriveGeneric, ScopedTypeVariables, FunctionalDependencies
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Mutable
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Flat, RUB-spline meshes.
-- 
-- Changelog:
--  + 12/02/2020  --  initial file;
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Mutable
  ( CCSMut (..)
  , symmExtend
  , unsafeSymmExtend
  , extendAdd
  , unsafeExtendAdd
  , unsafeAddInto
    -- Conversions and helpers:
  , mutant
  , retard
    -- Testing & examples:
  , testMutable
  ) where

import GHC.Generics (Generic)
import GHC.Types (SPEC(..))
import Control.Monad (when)
import Control.Monad.Primitive
import Data.Foldable (toList)
import Data.VectorSpace
import Data.Vector.Extras
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Permute  as Vec
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable.Mutable as Mut
import qualified Data.Vector.Algorithms.Intro as Mut

import Control.Monad.IO.Class
import Control.Logging
import Data.Text (pack)
import Text.Printf

import Data.CCSMat


-- * Mutable data types
------------------------------------------------------------------------------
data CCSMut (m :: * -> *) a =
  CCX { _xrowNum :: !Int
      , _xrowIdx :: !(MVector (PrimState m) Z)
      , _xcolPtr :: !(MVector (PrimState m) Z)
      , _xmatDat :: !(MVector (PrimState m) a)
      } deriving (Generic)


-- * Mutating operations
------------------------------------------------------------------------------
-- | Extend the rows and columns "symmetrically". This function allocates any
--   space that is required so that the (row, column)-pairs, corresponding to
--   the indices within the given vector, have space allocated to them.
--   
--   Algorithm:
--   
--    1. foreach index, `j \in js`:
--    2.   fetch column `j` of CCS;
--    3.   for each index, `k \in js`:
--    4.     if CCS(j,k) is missing, then:
--    5.       increment counter for new entries to insert;
--   
symmExtend ::
     PrimMonad m         -- required for mutable operations
  => MonadIO m           -- TODO: JUST FOR DEBUGGERING !!
  => Storable a          -- @Storable@ elements within the matrix
  => AdditiveGroup a     -- so that "zero(V)" elements can be inserted
  => CCSMut m a          -- mutable CCS sparse-matrix
  -> Vector Z            -- (row, col)-indices
  -> m (CCSMut m a)      -- updated matrix with zeroes inserted
symmExtend mat js = do
  debug' "---"
  debug' "Beginning 'symmExtend'"
  debug' "---"
  ar <- Vec.thaw js
  Mut.sort ar
  debug' "symmExtend: sorted"
  frz ar >>= unsafeSymmExtend mat
{-# INLINE[2] symmExtend #-}

showm :: CCSMat a -> String
showm (CCS rn ri cp _) = show (CCS rn ri cp Vec.empty :: CCSMat R)

------------------------------------------------------------------------------
-- | Assumes that the vector of indices has already been sorted.
unsafeSymmExtend ::
     forall m a.
     PrimMonad m         -- required for mutable operations
  => MonadIO m           -- TODO: JUST FOR DEBUGGERING !!
  => Storable a          -- @Storable@ elements within the matrix
  => AdditiveGroup a     -- so that "zero(V)" elements can be inserted
  => CCSMut m a          -- mutable CCS sparse-matrix
  -> Vector Z            -- (row, col)-indices
  -> m (CCSMut m a)      -- updated matrix with zeroes inserted
unsafeSymmExtend mat@(CCX _ ri cp _) js = do
  retard mat >>= debug' . pack . printf "source matrix:\n%s" . showm
  debug' . pack . printf "patch indices: %s" $ show js

  let nj  = len js
      nc  = Mut.length cp - 1

      -- Iterate over each column, `js[j]`, and return the total number of
      -- elements that need to be inserted into the matrix.
      -- Inputs:
      --  + `k` -- matrix column index;
      --  + `j` -- vector column pointer (to index in `js`); and
      --  + `c` -- number-new-elements count so far.
      goc :: Int -> Int -> Int -> m Int
      goc k j c
        | j    >= nj = do
            debug' "goc: done"
            pure c
        | js!j >= nc = do
            debug' "goc: done"
            pure $ c + nj*(nj-j) -- ran out of matrix columns
        | k < js!j   = do
            debug' "goc: next col"
            goc k' j c           -- skip to next column
        | otherwise  = do
            debug' "goc: merging zeroes into row"
            p <- rd cp (js!j)
            q <- rd cp (js!j + 1)
            gor p q 0 c >>= goc k' j'
        where
          j' = j+1
          k' = k+1

      -- Traverse the matrix-column (with indices `ri[p:q-1]`), and count the
      -- number of missing elements.
      gor :: Int -> Int -> Int -> Int -> m Int
      gor p q l c
        | l >= nj   = pure c
        | p >= q    = pure $ c + nj - l
        | otherwise = do
            i <- rd ri p
            let c' | js!l < i  = c+1
                   | otherwise = c
                l' | js!l > i  = l
                   | otherwise = l+1
                p' | js!l < i  = p
                   | otherwise = p+1
            gor p' q l' c'

  debug' "Beginning 'unsafeSymmExtend'"

  cc <- goc 0 0 0
  dbg $ printf "goc: num new entries = %d" cc

  if cc > 0
    then do mat' <- unsafeSymmZeroesN mat js cc
            debug' "Completed 'unsafeSymmExtend'"
            pure mat'
    else pure mat
{-# INLINE[1] unsafeSymmExtend #-}

------------------------------------------------------------------------------
-- | Assumes that the vector of indices has already been sorted.
unsafeSymmZeroesN ::
     forall m a.
     PrimMonad m         -- required for mutable operations
  => MonadIO m           -- TODO: JUST FOR DEBUGGERING !!
  => Storable a          -- @Storable@ elements within the matrix
  => AdditiveGroup a     -- so that "zero(V)" elements can be inserted
  => CCSMut m a          -- mutable CCS sparse-matrix
  -> Vector Z            -- (row, col)-indices
  -> Z                   -- number of zeroes to insert
  -> m (CCSMut m a)      -- updated matrix with zeroes inserted
unsafeSymmZeroesN (CCX rn ri cp xs) js cc = do
  -- Calculate some sizes:
  let nc' = (lst js+1) `max` nc -- new #columns
      rn' = (lst js+1) `max` rn -- new #rows
      nj  = len js
      nc  = Mut.length cp - 1   -- old #columns

  debug' "Beginning 'unsafeSymmZeroesN'"

  -- Allocate new arrays:
  rr <- new (Mut.length ri + cc)
  cr <- new (nc' + 1)
  xr <- new (Mut.length xs + cc)

  let -- Iterate over each column, `js[j]`, filling in row-indices and zero
      -- elements that need to be inserted into the matrix.
      -- Inputs:
      --  + `k` -- matrix column index;
      --  + `j` -- vector column pointer (to index in `js`); and
      --  + `p` -- current pointer into the new matrix data.
      goc :: Int -> Int -> Int -> m ()
      goc k j p
        | k >= nc'  = do        -- finished
            wr cr k p
            debug' "goc: done"
        | j >= nj   = do        -- copy over rest of matrix
            wr cr k p
            debug' "goc: copying old-matrix"
            o <- rd cp k
            Mut.drop p rr `Mut.copy` Mut.drop o ri
            Mut.drop p xr `Mut.copy` Mut.drop o xs
            let n = p - o
            dbg $ printf "goc: offsetting CPs by %d" n
            Vec.mapM_ (\l -> rd cp l >>= wr cr l . (+n)) $ efn k' (nc' - k)
            debug' "goc: finished"
        | k >= nc   = do        -- ran out of matrix columns
            wr cr k p
            debug' "goc: writing new columns"
            if k == js!j
              then do Mut.slice p nj rr `Vec.copy` js
                      Mut.slice p nj xr `Mut.set` zeroV
                      goc k' j' (p+nj)
              else goc k' j p
        | k == js!j = do      -- merge old column and zeroes
            wr cr k p
            debug' "goc: column merge"
            o <- rd cp k
            q <- rd cp k'
            gor p o q 0 >>= goc k' j'
        | otherwise = do      -- copy old matrix column over
            wr cr k p
            debug' "goc: column copy"
            o <- rd cp k
            q <- rd cp k'
            let n = q - o
            Mut.slice p n rr `Mut.copy` Mut.slice o n ri
            Mut.slice p n xr `Mut.copy` Mut.slice o n xs
            goc k' j (p+n)
        where
          j' = j+1
          k' = k+1

      -- Merge the original matrix column with the "zeroes column" to give the
      -- updated column of the matrix.
      -- Inputs:
      --  + `p` -- current pointer into new matrix arrays;
      --  + `o` -- the "start-pointer" for the old matrix;
      --  + `q` -- the "end-pointer" for the old matrix (e.g., so row indices
      --           would be `ri[o:q-1]`); and
      --  + `l` -- pointer into `js`.
      -- The output of this function is the updated value of the arrays write-
      -- pointer.
      gor :: Int -> Int -> Int -> Int -> m Int
      gor p o q l
        | l >= nj   = do        -- all zeroes set, so copy old matrix data
            let n = q - o
            Mut.slice p n rr `Mut.copy` Mut.slice o n ri
            Mut.slice p n xr `Mut.copy` Mut.slice o n xs
            pure (p + n)
        | o >= q    = do        -- all old matrix data copied, so zero-fill
            let n = nj - l
            Mut.slice p n rr `Vec.copy` slc l n js
            Mut.slice p n xr `Mut.set` zeroV
            pure (p + n)
        | otherwise = do        -- copy or zero an entry
            i <- rd ri o
            when (js!l >= i) $ do
              wr rr p i
              rd xs o >>= wr xr p
            when (js!l < i) $ do
              wr rr p (js!l)
              wr xr p zeroV
            let l' | js!l > i  = l
                   | otherwise = l+1
                o' | js!l < i  = o
                   | otherwise = o+1
            gor (p+1) o' q l'

  _ <- goc 0 0 0
  debug' "Completed 'unsafeSymmZeroesN'"

  pure $ CCX rn' rr cr xr
{-# INLINE[2] unsafeSymmZeroesN #-}

------------------------------------------------------------------------------
extendAdd ::
     PrimMonad m         -- required for mutable operations
  => MonadIO m           -- TODO: JUST FOR DEBUGGERING !!
  => Functor f
  => Foldable f
  => Storable a          -- @Storable@ elements within the matrix
  => AdditiveGroup a     -- so that "zero(V)" elements can be inserted
  => CCSMut m a          -- mutable CCS sparse-matrix
  -> Vector Z            -- (row, col)-indices
  -> f (Vector a)        -- @Foldable@ set of columns of a full matrix
  -> m (CCSMut m a)      -- updated matrix with zeroes inserted
extendAdd mat js xs = do
  debug' "---"
  debug' "Beginning 'extendAdd'"
  debug' "---"
  let p   = Vec.sortperm js
      js' = js `bkp` p
      xs' = flip bkp p <$> xs
  js' `seq` debug' "extendAdd: sorted"
  unsafeExtendAdd mat js' xs'
{-# INLINE[2] extendAdd #-}

unsafeExtendAdd ::
     forall m f a.
     PrimMonad m         -- required for mutable operations
  => MonadIO m           -- TODO: JUST FOR DEBUGGERING !!
  => Foldable f
  => Storable a          -- @Storable@ elements within the matrix
  => AdditiveGroup a     -- so that "zero(V)" elements can be inserted
  => CCSMut m a          -- mutable CCS sparse-matrix
  -> Vector Z            -- (row, col)-indices
  -> f (Vector a)        -- @Foldable@ set of columns of a full matrix
  -> m (CCSMut m a)      -- updated matrix with zeroes inserted
unsafeExtendAdd mat js xs = do
  mat' <- unsafeSymmExtend mat js
  unsafeAddInto mat' js xs
{-# INLINE unsafeExtendAdd #-}

------------------------------------------------------------------------------
-- | Adds the given matrix-columns into the sparse-matrix, using the symmetric
--   indices, `js`.
--   
--   NOTE: assumes that the elements exist already in the sparse-matrix, and
--     will fail if this is not the case.
--   
unsafeAddInto ::
     forall m f a.
     PrimMonad m         -- required for mutable operations
--   => MonadIO m           -- TODO: JUST FOR DEBUGGERING !!
  => Foldable f
  => Storable a          -- @Storable@ elements within the matrix
  => AdditiveGroup a     -- so that "zero(V)" elements can be inserted
  => CCSMut m a          -- mutable CCS sparse-matrix
  -> Vector Z            -- (row, col)-indices
  -> f (Vector a)        -- @Foldable@ set of columns of a full matrix
  -> m (CCSMut m a)      -- updated matrix with zeroes inserted
unsafeAddInto mat@(CCX _ rr cr xr) js xs = do
  let nj = len js

      -- Inputs:
      --  + `l`  -- pointer into `js`; and
      --  + `vs` -- non-zero vector elements.
      goc :: Int -> Vector a -> m ()
      goc l vs = rd cr (js!l) >>= \p -> gor SPEC p 0 vs
      {-# INLINE goc #-}

      -- Add the `(js,vs)` sparse vector into the given matrix column.
      -- Inputs:
      --  + `p`  -- current pointer into the matrix arrays;
      --  + `r`  -- pointer into `js`; and
      --  + `vs` -- non-zero vector elements.
      gor :: SPEC -> Int -> Int -> Vector a -> m ()
      gor !_ p r vs
        | r >= nj   = pure ()
        | otherwise = do
            i <- rd rr p
            if js!r == i
              then rd xr p >>= wr xr p . (vs!r ^+^) >> gor SPEC p' (r+1) vs
              else gor SPEC p' r vs
        where
          p' = p+1

  mapM_ (uncurry goc) $ [0..] `zip` toList xs
  pure mat
{-# INLINE unsafeAddInto #-}


-- * Conversions and helpers
------------------------------------------------------------------------------
-- | Make a CCS sparse-matrix mutable by "thawing" its data arrays.
mutant :: (PrimMonad m, Storable a) => CCSMat a -> m (CCSMut m a)
mutant (CCS rn ri cp xs) = CCX rn <$> thw ri <*> thw cp <*> thw xs
{-# INLINE mutant #-}

-- | Make a CCS sparse-matrix immutable by "freezing" its data arrays.
retard :: (PrimMonad m, Storable a) => CCSMut m a -> m (CCSMat a)
retard (CCX rn rr cr xr) = CCS rn <$> frz rr <*> frz cr <*> frz xr
{-# INLINE retard #-}

-- ** Debugging helpers
------------------------------------------------------------------------------
dbg :: MonadIO m => String -> m ()
dbg  = debug' . pack
{-# INLINE dbg #-}


-- * Testing & examples
------------------------------------------------------------------------------
testMutable :: IO ()
testMutable  = do
  let im = eye 8 :: CCSMat Double
      zx = zeroV :: CCSMat Double
      js = vec [0, 5, 7, 9]
      xs = 4 `replicate` vec [1.0, 2.0, 0.5, 3.1]
  printCCS im "Im"
  print im
  mu <- mutant im
--   se <- symmExtend mu js
  se <- extendAdd mu js xs
  nw <- retard se
  printCCS nw "Nw"
  print nw

  nz <- mutant zx >>= \freak -> extendAdd freak js xs >>= retard
  printCCS nz "Freak"
  print nz
