{-# LANGUAGE BangPatterns, TupleSections #-}
module Data.CCSMat.NewMul
  ( mul
  , mulMap
  , mulSlow
  , unsafeMul
  , mulSpMV
  ) where

-- FIXME: Not robust, better checking of input-matrix shapes is needed.

-- TODO: Currently `saxpy' mutates it's 3rd argument, the `shape' matrix.
--   A cleaner way of reusing shapes needs to be implemented.
-- TODO: Sparse-matrix multiplication, for matrices of the same shape each 
--   time, should be very quick when using appropriate accleleration data-
--   structures.

import GHC.Types (SPEC(..))
import Control.Monad.ST
import Control.Monad.Primitive
import Data.STRef
import Data.Bool
-- import Data.IntMap (IntMap)
-- import qualified Data.IntMap as IntMap
-- import Data.IntMap.Strict (IntMap)
-- import qualified Data.IntMap.Strict as IntMap
import qualified Data.List as List
import Data.Vector.Helpers
import Data.Vector.Storable(Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Storable.Mutable as M

import Data.CCSMat.Internal
import Data.CCSMat.Transpose
import Data.CCSMat.Multiply (multiply)
import Data.CCSMat.Sort
import Data.CCSMat.MapMat
import Data.CCSMat.Vector
import Data.Vector.Sparse


------------------------------------------------------------------------------
-- | Calculate the product of two sparse-matrices one column at a time.
--   
--   NOTE:
--    + Multiplication is "in-place" and matrix @c@ is also the output.
--    + Assumes that matrix A has been transposed.
--   
saxpyT :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a -> CCSMat a
saxpyT a b c@(CCS rn ri cp xs) = runST $ do
  ri' <- Vec.unsafeThaw ri
  xs' <- Vec.unsafeThaw xs
  cnz <- newSTRef 0 :: ST s (STRef s Int)
  let ks = 0 `efn` getColN c :: Vector Z
  Vec.forM_ ks $ \k -> do
    let sk = symCol b k
        dk = fullCol b k
        js = 0 `efn` getColN a :: Vector Z
    Vec.forM_ js $ \j -> do
      let SparseVector _ ai ax = sparseCol a j -- TODO: Use `slicePtrs'?
          nz = Vec.any id $ bkp sk ai -- TODO: Make faster.
      case nz of
        True -> do               -- TODO: Only multiply the non-zero elements.
          let qs = 0 `efn` len ai :: Vector Z
          dp <- newSTRef 0
          Vec.forM_ qs $ \q -> do  -- Dot-product: cjk := a_j * b_k
            let i = ai!q
                x = ax!q
                p = x * (dk!i)
            modifySTRef dp (+p)
          ptr <- readSTRef cnz
          wr ri' ptr j
          readSTRef dp >>= wr xs' ptr
          writeSTRef cnz (ptr+1)
        False -> return ()
    -- endfor
  -- endfor
  Vec.unsafeFreeze ri'
  Vec.unsafeFreeze xs'
  return c
{-# SPECIALIZE saxpyT :: CCSMat R -> CCSMat R -> CCSMat R -> CCSMat R #-}


------------------------------------------------------------------------------
-- | Matrix multiplication, implemented using more standard Haskell code, and
--   libraries.
--   
--   TODO: Currently this function doesn't do any sanity-checking, of its
--     inputs.
mul :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
-- mul  = mulMap
mul  = error "FIXME: currently breaks when multiplying non-square matrices."
{-# SPECIALIZE mul :: CCSMat R -> CCSMat R -> CCSMat R #-}

------------------------------------------------------------------------------
-- | Uses `CCSMat.Vector.spmv`.
mulSpMV :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
mulSpMV x = fromSparseCols . map (spmspv x) . toSparseCols
{-# SPECIALIZE mulSpMV :: CCSMat R -> CCSMat R -> CCSMat R #-}

------------------------------------------------------------------------------
-- | Multiplies two matrices, using the algorithm:
--     C(:,j) += A(:,k) * B(k,j) ,   j in [0,n-1]
--   
unsafeMul :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
unsafeMul x = fromSparseCols . map (mulCol x) . toSparseCols
{-# SPECIALIZE unsafeMul :: CCSMat R -> CCSMat R -> CCSMat R #-}

mulSlow :: (Num a, Ord a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
mulSlow x y = sort x `unsafeMul` sort y

infixl 6 ^+^
infixl 7 *^

(^+^) :: (Num a, Storable a) => SparseVector a -> SparseVector a -> SparseVector a
{-# INLINE (^+^) #-}
-- (^+^)  = unsafeVecAdd
-- (^+^)  = vecAddMap
(^+^)  = csVecAdd
-- (^+^) :: (Num a, Eq a, Storable a) => SparseVector a -> SparseVector a -> SparseVector a
-- (^+^)  = vecAdd

(*^) :: (Num a, Storable a) => a -> SparseVector a -> SparseVector a
{-# INLINE (*^) #-}
(*^)  = vecScale

-- mulCol :: (Num a, Eq a, Storable a) => CCSMat a -> SparseVector a -> SparseVector a
mulCol' :: (Num a, Storable a) => CCSMat a -> SparseVector a -> SparseVector a
{-# SPECIALIZE mulCol' :: CCSMat Double -> SpVD -> SpVD #-}
mulCol' m (SparseVector n ix vs) =
  let ps = 0 `efn` len ix :: Vector Z
      v0 = SparseVector n mempty mempty
  in  Vec.foldl' (\v p -> (vs!p) *^ sparseCol m (ix!p) ^+^ v) v0 ps

mulCol :: (Num a, Storable a) => CCSMat a -> SparseVector a -> SparseVector a
{-# SPECIALIZE mulCol :: CCSMat Double -> SpVD -> SpVD #-}
mulCol (CCS rn ri cp xs) (SparseVector n ix vs) =
  let ps = 0 `efn` len ix :: Vector Z
      ls = Vec.zipWith (-) (tal cp) cp
      v0 = SparseVector n mempty mempty
      go p =
        let j = ix!p
            q = cp!j
            l = ls!j
        in  SparseVector rn (slc q l ri) (Vec.map (*vs!p) $ slc q l xs)
  in  Vec.foldl' (\v p -> go p ^+^ v) v0 ps


-- * Testing and examples.
------------------------------------------------------------------------------
multiplyTest = do
  let m  = CCS 2 ri cp xs
      ri = Vec.fromList [0,1,1,0]
      cp = Vec.fromList [0,2,4]
      xs = Vec.fromList [-4,-3,4,3] :: Vector Int
  print m
  print $ m `multiply` m
  print $ m `mul` m
  print $ m `unsafeMul` m
