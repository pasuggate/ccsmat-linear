{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, GADTs, TypeOperators,
             OverloadedLists, TupleSections, BangPatterns, InstanceSigs,
             MultiParamTypeClasses, TypeFamilies, FunctionalDependencies,
             FlexibleContexts, FlexibleInstances, TypeSynonymInstances,
             DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
             StandaloneDeriving, GeneralizedNewtypeDeriving,
             ViewPatterns, PatternSynonyms, ScopedTypeVariables,
             TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.VectorSpace
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Vector space instances
-- 
-- Changelog:
--  + 18/11/2018  --  initial file;
-- 
-- TODO:
--  + move this to an appropriate library; e.g., `ccsmat-linear`?
-- 
------------------------------------------------------------------------------

module Data.CCSMat.VectorSpace
  ( SpMV (..)
  , vsxspm
  , spmxvs
  , spmvs
  ) where

import Control.Monad.ST
import Data.List (foldl')
import Data.VectorSpace
import Data.VectorSpace.Orphans
import Data.Vector.Storable (Storable, Vector)
import qualified Data.Vector.Storable as Vec
import Data.Vector.Helpers
import Data.CCSMat.Internal as CCS
import Data.CCSMat.Vector (SpMV (..))
import qualified Data.CCSMat.Vector as CCS
import qualified Data.CCSMat.Addsub as CCS
import Linear (V1 (..), V2 (..), V3 (..), V4 (..))
import Text.Printf


-- * VectorSpace orphan instances
------------------------------------------------------------------------------
{-- }
-- NOTE: better implementation has been moved to `Data.CCSMat.Addsub`.
instance (Storable a, Num a, Scalar a ~ a) => AdditiveGroup (CCSMat a) where
--   zeroV = CCS 0 Vec.empty Vec.empty Vec.empty
  zeroV = CCS 0 Vec.empty (Vec.singleton 0) Vec.empty
  negateV (CCS rn ri cp xs) = CCS rn ri cp $ Vec.map negate xs
  CCS 0 _ _ _ ^+^ b = b
  a ^+^ CCS 0 _ _ _ = a
  a ^+^ b = CCS.add a b
  CCS 0 _ _ _ ^-^ b = negateV b
  a ^-^ CCS 0 _ _ _ = a
  a ^-^ b = CCS.sub a b
  {-# INLINE zeroV #-}
  {-# INLINE negateV #-}
  {-# INLINE (^+^) #-}
  {-# INLINE (^-^) #-}

instance (Scalar a ~ a, Num a, Storable a) => VectorSpace (CCSMat a) where
  type Scalar (CCSMat a) = a
  (*^) = CCS.scale
  {-# INLINE (*^) #-}
--}

------------------------------------------------------------------------------
instance SpMV (Vector (V1 R)) R where
  spmv = spmxvs
  vspm = vsxspm
  {-# INLINE spmv #-}
  {-# INLINE vspm #-}

instance SpMV (Vector (V2 R)) R where
  spmv = spmxvs
  vspm = vsxspm
  {-# INLINE spmv #-}
  {-# INLINE vspm #-}

instance SpMV (Vector (V3 R)) R where
  spmv = spmxvs
  vspm = vsxspm
  {-# INLINE spmv #-}
  {-# INLINE vspm #-}

instance SpMV (Vector (V4 R)) R where
  spmv = spmxvs
  vspm = vsxspm
  {-# INLINE spmv #-}
  {-# INLINE vspm #-}


-- * Additional `VectorSpace` operations for `CCSMat`
------------------------------------------------------------------------------
-- | Vector (of a `VectorSpace`) times a sparse matrix.
vsxspm :: (s ~ Scalar a, Storable s, Storable a, VectorSpace a) =>
          Vector a -> CCSMat s -> Vector a
vsxspm vs m@(CCS rn _ _ _)
  | len vs == rn = Vec.fromList $ CCS.withCols' go m
  | len vs ==  0 = vs
  | otherwise    = error msg
  where
    msg = printf "vsxspm: invalid vector (%d) and matrix (rows: %d) dimensions" (len vs) rn
    go _ ri xs = bkp vs ri `dot` xs
    dot = (Vec.foldl1' (^+^) .) . Vec.zipWith (^*)
    {-# INLINE dot #-}
{-# INLINABLE  vsxspm #-}
{-# SPECIALIZE vsxspm :: Vector (V1 R) -> CCSMat R -> Vector (V1 R) #-}
{-# SPECIALIZE vsxspm :: Vector (V2 R) -> CCSMat R -> Vector (V2 R) #-}
{-# SPECIALIZE vsxspm :: Vector (V3 R) -> CCSMat R -> Vector (V3 R) #-}
{-# SPECIALIZE vsxspm :: Vector (V4 R) -> CCSMat R -> Vector (V4 R) #-}

------------------------------------------------------------------------------
-- | Sparse-matrix times vector of vector-space elements.
--   
--   TODO: unfinished, and untested.
--   
spmvs ::
     Storable a
  => VectorSpace a
  => Scalar a ~ R
  => CCSMat R
  -> Vector a
  -> Vector a
spmvs (CCS rn ri cp xs) vec = runST $ do
  ar <- thw $ rn `rep` zeroV
  let col j p  = do
        let s  = vec!j
            q  = cp!j'
            j' = j + 1
        efn p (q-p) `Vec.forM_` \k -> do
          let i = ri!k
          x <- rd ar i
          wr ar i (x ^+^ xs!k *^ s)
        pure j'
      {-# INLINE col #-}
  Vec.foldM'_ col 0 (nit cp)
  frz ar
{-# INLINABLE  spmvs                                               #-}
{-# SPECIALIZE spmvs :: CCSMat R -> Vector     R  -> Vector     R  #-}
{-# SPECIALIZE spmvs :: CCSMat R -> Vector (V1 R) -> Vector (V1 R) #-}
{-# SPECIALIZE spmvs :: CCSMat R -> Vector (V2 R) -> Vector (V2 R) #-}
{-# SPECIALIZE spmvs :: CCSMat R -> Vector (V3 R) -> Vector (V3 R) #-}
{-# SPECIALIZE spmvs :: CCSMat R -> Vector (V4 R) -> Vector (V4 R) #-}

------------------------------------------------------------------------------
-- | Sparse matrix times (dense) vector.
--   
--   TODO: check if vector-/list- fusion is working.
spmxvs :: (s ~ Scalar a, Storable s, Storable a, VectorSpace a) =>
          CCSMat s -> Vector a -> Vector a
spmxvs m@(CCS rn _ cp _) vs
  | len vs == cn = foldl' add zs $ CCS.withCols' go m
  | len vs ==  0 = vs
  | otherwise    = error msg
  where
    msg = printf "spmxvs: invalid vector (%d) and matrix (cols: %d) dimensions" (len vs) cn
    go j ri xs = (ri, Vec.map (vs!j ^*) xs)
    zs  = rn `rep` zeroV
    cn  = len cp - 1
    add s = uncurry $ acc_ (^+^) s
    {-# INLINE add #-}
{-# INLINABLE  spmxvs #-}
{-# SPECIALIZE spmxvs :: CCSMat R -> Vector (V1 R) -> Vector (V1 R) #-}
{-# SPECIALIZE spmxvs :: CCSMat R -> Vector (V2 R) -> Vector (V2 R) #-}
{-# SPECIALIZE spmxvs :: CCSMat R -> Vector (V3 R) -> Vector (V3 R) #-}
{-# SPECIALIZE spmxvs :: CCSMat R -> Vector (V4 R) -> Vector (V4 R) #-}
