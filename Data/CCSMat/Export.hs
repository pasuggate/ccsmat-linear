{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, TupleSections,
             FlexibleContexts, BangPatterns, RecordWildCards,
             MultiParamTypeClasses, FunctionalDependencies, OverloadedStrings,
             FlexibleInstances, TypeSynonymInstances,
             GeneralizedNewtypeDeriving,
             StandaloneDeriving,
             DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric,
             GADTs, KindSignatures, DataKinds, PolyKinds,
             TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances,
             CPP, TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Export
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Miscellaneous vector linear-algebra functions.
--
-- NOTE:
-- 
-- Changelog:
--  + 11/06/2017  -- initial file (refactored from elsewhere);
-- 
-- TODO:
--  + include other conversions;
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Export
       ( hmatrix
       , ccsToSVG
       , ccsToSVGusingLines
       , renderToFile
       ) where

-- HMatrix library imports:
-- import Prelude hiding (id, (.), const, fst, snd)
-- import Control.Category.Prelude
import Control.Arrow (arr, (<<<), (&&&))
import Numeric.LinearAlgebra (Matrix)
import qualified Numeric.LinearAlgebra as H
import Data.Text as Text
import Text.Printf
import Graphics.Svg as Svg

import Data.Vector.Helpers as Help
import Data.Vector.Storable (Storable)
import Data.CCSMat (CCSMat(..))
import qualified Data.CCSMat as CCS
import qualified Data.CCSMat.Stream as CCS

import Data.CCSMat.Element (Elem (..))


-- * Miscellaneous `hmatrix` functions.
------------------------------------------------------------------------------
-- | Convert a sparse matrix into an `hmatrix` matrix.
hmatrix :: (H.Transposable (Matrix a) (Matrix a), H.Element a, Num a) =>
           CCSMat a -> Matrix a
{-# SPECIALIZE hmatrix :: CCSMat R -> Matrix R #-}
-- hmatrix  = arc H.reshape <<< CCS.getColN &&& CCS.full
hmatrix  = H.tr' . arr (uncurry H.reshape) <<< CCS.getRowN &&& CCS.full


-- ** SVG exporters for sparse matrices
------------------------------------------------------------------------------
ccsToSVG :: Storable a => CCSMat a -> Svg.Element
ccsToSVG mat =
  let (n, m) = (CCS.getRowN mat, CCS.getColN mat)
      (h, w) = (n*8, m*8)
      go i j = rect_ [ X_ <<- txt (j*8+1), Y_ <<- txt (i*8+1)
                     , Width_ <<- "7", Height_ <<- "7", Fill_ <<- "#ff7f00"]
      mtx = mconcat [go i j | Elem i j _ <- CCS.toElems mat]
      bg  = rect_ [ Width_ <<- "100%", Height_ <<- "100%", "white" ->> Fill_]
  in  succ w `svgSize` succ h $ bg <> mtx

ccsToSVGusingLines :: Storable a => CCSMat a -> Svg.Element
ccsToSVGusingLines mat =
  let (n, m) = (CCS.getRowN mat, CCS.getColN mat)
      (h, w) = (n*8, m*8)
      go i j = path_ [ Fill_ <<- "#ff7f00"
                     , D_    <<- (mA x y <> lA x (y-6) <> lA (x+6) (y-6) <>
                                  lA (x+6) y <> lA x y <> z <> mA x y) ]
        where (x, y) = (zr (j*8+1), zr (h - i*8-1))
      mtx = mconcat [go i j | Elem i j _ <- CCS.toElems mat]
      bg  = rect_ [ Width_ <<- "100%", Height_ <<- "100%", "white" ->> Fill_]
  in  svgSize w h $ bg <> mtx


-- * Helper functions
------------------------------------------------------------------------------
-- Textify
txt :: Z -> Text
txt  = Text.pack . show
{-# INLINE txt #-}

-- | Realise an integer.
zr :: Z -> R
zr  = fromIntegral
{-# INLINE zr #-}

------------------------------------------------------------------------------
-- | Set the document size
svgSize :: Z -> Z -> Svg.Element -> Svg.Element
svgSize w h content
  = doctype <> with (svg11_ content) [ Version_ <<- "1.1"
                                     , Width_   <<- pack (printf "%d" w)
                                     , Height_  <<- pack (printf "%d" h)
                                     ]
