{-# LANGUAGE BangPatterns, Rank2Types, GADTs, FlexibleContexts, DeriveFunctor,
             ScopedTypeVariables
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Vector.Sparse.Stream
-- Copyright   : (C) Patrick Suggate, 2015
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Stream-fusion functionality, for sparse matrices.
-- 
-- Changelog:
--  + 27/08/2015  --  initial file;
-- 
-- TODO:
--  + monadic streams;
--  + flattening of nested streams?
--  + set-ops; e.g., union, etc.?
--  + `SPEC` specialisations?
--  + look at (the fusion in) Kmett's `sparse` library;
--  + what use-cases allow for SIMD?
-- 
------------------------------------------------------------------------------

module Data.Vector.Sparse.Stream
       ( runS
       , unstream
       , elemS
       , elemW
       , indiceS
       , filtM
       , El(..)
       , vectorStream
       , testStream
       ) where

import Prelude hiding (Maybe(..))
import GHC.Types (SPEC(..))
import Control.Monad.Primitive
import qualified Data.Vector.Generic as G
import Data.Vector.Storable (Vector, Storable)

import Data.Vector.Fusion.Stream.Monadic (Stream(..), Step(..))
import qualified Data.Vector.Fusion.Stream.Monadic as Stream

import Data.Vector.Sparse
import Data.Vector.Helpers


-- * Types useful for streaming.
------------------------------------------------------------------------------
-- | Matrix elements.
data El a = El { idx :: {-# UNPACK #-} !Z, elm :: !a } deriving (Eq, Show, Functor)
-- data El a = El { idx :: !Z, elm :: !a } deriving (Eq, Show, Functor)
-- data El a = El { idx :: !Z, elm :: a } deriving (Eq, Show, Functor)
-- data El a = El { idx :: Z, elm :: a } deriving (Eq, Show, Functor)


-- * Stream evaluation.
------------------------------------------------------------------------------
-- | Evaluate to a list.
runS :: Monad m => Stream m a -> m [a]
runS (Stream step t) = go SPEC t
  where
    go !_ s = step s >>= \r -> case r of
      Yield x s' -> (x:) <$> go SPEC s'
      Skip    s' -> go SPEC s'
      Done       -> return []
{-# INLINE [1] runS #-}

------------------------------------------------------------------------------
-- | Convert the stream to a vector.
unstream ::
     forall m a v.
     PrimMonad m
  => G.Vector v a
  => Z
  -> Stream m a
  -> m (v a)
unstream n (Stream step t) = do
  ar <- new n
  let go !_ !p s = step s >>= \r -> case r of
        Yield x s' -> wr ar p x >> go SPEC (p+1) s'
        Skip    s' -> go SPEC p s'
        Done       -> frz ar
  go SPEC 0 t
{-# INLINE [1] unstream #-}


-- * Sparse-vector streams.
------------------------------------------------------------------------------
-- | Use the given accessor- and predicate- functions to construct a stream of
--   sparse-vector elements.
elemW ::
     Monad m
  => Storable a
  => (Z -> a -> b)
  -> (Z -> a -> Bool)
  -> SparseVector a
  -> Stream m b
elemW f g (SparseVector _ ri xs) = Stream step 0
  where
    step !p
      | p  >=  nz = return   Done
      | g i x     = return $ Yield (f i x) p'
      | otherwise = return $ Skip          p'
      where p' = p+1
            i  = ri!p
            x  = xs!p
    {-# INLINE [0] step #-}
    nz = len ri
{-# INLINE [1] elemW #-}

elemS :: (Monad m, Storable a) => SparseVector a -> Stream m (El a)
elemS  = elemW El tt
  where
    tt _ = const True
    {-# INLINE [0] tt #-}
{-# INLINE [1] elemS #-}

------------------------------------------------------------------------------
-- | Construct a stream of indices.
indiceS :: (Monad m, Storable a) => SparseVector a -> Stream m Z
indiceS  = elemW go tt
  where
    go !i = const i
    tt  _ = const True
    {-# INLINE [0] go #-}
    {-# INLINE [0] tt #-}
{-# INLINE [1] indiceS #-}


-- * Operations on sparse-vectors, using streams.
------------------------------------------------------------------------------
-- | Apply the given filtering-function, to a sparse matrix.
filtM :: (Storable a, PrimMonad m) =>
         (Z -> a -> Bool) -> SparseVector a -> m (SparseVector a)
filtM f v@(SparseVector rn _ _) = streamSparse rn (elemW El f v)
{-# INLINE [1] filtM #-}


-- * Build sparse-vector streams.
------------------------------------------------------------------------------
-- | Filter out the zeros, to build a sparse-vector stream.
vectorStream :: (Num a, Eq a, Applicative m, G.Vector v a) =>
                v a -> Stream m (El a)
vectorStream v =
  let n = len v
      step p | p  >=  n  = pure   Done
             | x  ==  0  = pure $ Skip             p'
             | otherwise = pure $ Yield (El p x) p'
        where p' = p+1
              x  = v!p
      {-# INLINE [0] step #-}
  in  Stream step 0
{-# INLINE [1] vectorStream #-}

------------------------------------------------------------------------------
-- | Filter out the zeros, to build a sparse-vector stream.
--   
--   TODO: Use `Bundle's, to pass size-info, and to allow differing chunk-
--     sizes.
streamSparse :: (PrimMonad m, Storable a) =>
                Z -> Stream m (El a) -> m (SparseVector a)
streamSparse rn ss@(Stream step t) = do
  nz <- Stream.length ss
  rr <- new nz
  xr <- new nz
  let go !_ !p s = step s >>= \r -> case r of
        Yield (El i x) s' ->
          wr rr p i >> wr xr p x >> go SPEC (p+1) s'
        Skip           s' -> go SPEC p s'
        Done              -> return ()
  go SPEC 0 t
  SparseVector rn <$> frz rr <*> frz xr
{-# INLINE [1] streamSparse #-}


-- * Testing and examples.
------------------------------------------------------------------------------
testStream :: IO ()
testStream  = do
  let ri = vec [0,1,4]
      xs = vec [1,2,3] :: Vector Double
      uu = SparseVector 5 ri xs
      rj = vec [2,4]
      ys = vec [4,5] :: Vector Double
      vv = SparseVector 5 rj ys
  print uu
  print =<< runS (elemS uu)
  print =<< runS (elemS vv)
