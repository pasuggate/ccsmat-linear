{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE Rank2Types, GADTs #-}
module Data.Vector.Sparse.Fusion.Monadic where


-- * Monadic fusion data-types.
------------------------------------------------------------------------------
-- | Breaks a recursive operation up into imperative "corecursive" ticks.
data Tick s a where
  Give :: a -> s -> Tick s a
  Tick ::      s -> Tick s a
--   Last ::           Tick s a
  Last :: a ->      Tick s a

-- | Tubes are monadic streams.
data Tube m a = forall s. Tube (s -> m (Tick s a)) s


instance Functor (Tick s) where
  {-# INLINE fmap #-}
  fmap f (Give x s) = Give (f x) s
  fmap _ (Tick   s) = Tick       s
--   fmap _  Done      = 
  fmap f (Last x  ) = Last (f x)

-- instance Monad m => Functor (Tube m) where
instance Functor m => Functor (Tube m) where
  {-# INLINE fmap #-}
  fmap f (Tube tick s) = Tube (fmap (fmap f) . tick) s


dotpS :: (Functor m, Num a) => Tube m a -> Tube m a -> m a
dotpS  = undefined

unionWithS :: Functor m => (a -> a -> a) -> Tube m a -> Tube m a -> Tube m a
unionWithS f s t = undefined


data Flg m a = Flg m a


squirtVec :: SparseVector a -> Tube m a
squirtVec (SparseVector rn ix xs) =
  let tick s = undefined
  in  Tube tick 0

shape :: Stream Int -> 
