{-# LANGUAGE BangPatterns, DeriveGeneric, OverloadedLists, PatternSynonyms
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Vector.Sparse
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : BSD-3-Clause
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Sparse vector data-types.
-- 
-- Changelog:
--  + 21/08/2015  --  initial file;
-- 
-- TODO:
--  + require that the sparse-vectors be sorted (with ascending indices)?
--  + benchmarking and determining the various inlining phases for each of
--    the functions?
-- 
------------------------------------------------------------------------------

module Data.Vector.Sparse
       ( SparseVector (..)
       , SpVD
       , SpV
       , pattern SpV
         -- Constructors:
       , fullUsing
       , sparseUsing
       , spv
         -- Queries:
       , spnnz
         -- Vector-space operations:
       , vecAdd
       , unsafeVecAdd
       , csVecAdd
       , vecAddMap
       , vecScale
         -- Modifiers and maps:
       , vecSort
       , vecSort'
       , sparseMap
       , vecFilt
       , vecFiltBy
         -- Sparse vector-specific functionality:
       , spdot
       , vspdot
       , spvdot
       , vspmul
       , spsum
         -- @IntMap@ sparse vectors:
       , vec2map
       , map2vec
       , vmadd
       , vmscale
       , sparseTest
         -- Mixed sparse/dense operations:
       , spvpv
       , vpspv
       ) where

import GHC.Types (SPEC(..))
import GHC.Generics (Generic)
import GHC.Exts (IsList (..))
import Control.Monad.ST
import Control.Arrow
import qualified Data.List as List
import Data.Monoid
-- import Data.IntMap (IntMap)
-- import qualified Data.IntMap as IntMap
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Storable.Mutable as Mut
import qualified Data.Vector.Algorithms.Intro as Mut
import Text.Printf

import Data.Vector.Helpers


-- * Sparse-vector data-types.
------------------------------------------------------------------------------
-- | Can be a row-, or a column-, vector.
data SparseVector a =
  SparseVector { elemNum :: Int, indices :: Vector Int, vecData :: Vector a }
  deriving (Eq, Show, Generic)

type SpVD  = SparseVector Double

type    SpV       a = SparseVector       a
pattern SpV n ri xs = SparseVector n ri xs


-- * Some standard instances
------------------------------------------------------------------------------
instance Storable a => Semigroup (SparseVector a) where
  (<>) = cat
  {-# INLINE (<>) #-}

instance Storable a => Monoid (SparseVector a) where
  mempty  = SparseVector 0 mempty mempty
  mappend = cat
  {-# INLINE    mempty  #-}
  {-# INLINE[2] mappend #-}

instance (Storable a, Num a) => Num (SparseVector a) where
  (+) = csVecAdd
  (-) = csVecWith (-)
  (*) = csVecWith (*)
  negate (SparseVector n ix xs) = SparseVector n ix $ Vec.map negate xs
  signum (SparseVector n ix xs) = SparseVector n ix $ Vec.map signum xs
  abs    (SparseVector n ix xs) = SparseVector n ix $ Vec.map abs    xs
  fromInteger = SparseVector 0 Vec.empty . Vec.singleton . fromInteger


-- * Convert to/from sparse representations.
------------------------------------------------------------------------------
fullUsing :: Storable a => a -> SparseVector a -> Vector a
{-# SPECIALIZE fullUsing :: Double -> SpVD -> Vector Double #-}
fullUsing z (SparseVector rn ri xs) =
  let zz = Vec.replicate rn z
  in  upd_ zz ri xs

sparseUsing :: (Eq a, Storable a) => a -> Vector a -> SparseVector a
{-# SPECIALIZE sparseUsing :: Double -> Vector Double -> SpVD #-}
sparseUsing z vs =
  let rn = len vs
      ri = Vec.findIndices (/= z) vs
      xs = Vec.filter (/= z) vs
  in  SparseVector rn ri xs

sparseMap :: (Storable a, Storable b) =>
             (a -> b) -> SparseVector a -> SparseVector b
-- {-# SPECIALIZE sparseMap :: (Double -> Double) -> SpVD -> SpVD #-}
{-# INLINE sparseMap #-}
sparseMap f (SparseVector rn ri xs) = SparseVector rn ri (Vec.map f xs)

------------------------------------------------------------------------------
-- | Construct a sparse vector from a length and a list of index-value
--   pairs.
spv :: (Storable a) => Z -> [(Z, a)] -> SparseVector a
spv rn = uncurry (SparseVector rn) . (Vec.fromList *** Vec.fromList) . unzip
{-# INLINE spv #-}


-- * Queries.
------------------------------------------------------------------------------
-- | Return the Number of Non-Zeroes (NNZ) of the given sparse vector.
spnnz :: SparseVector a -> Int
{-# INLINE spnnz #-}
spnnz  = len . indices


-- * Additional vector functions.
------------------------------------------------------------------------------
{-- }
vecUnion :: Storable a => Vector a -> Vector a -> Vector a
vecUnion v w = runST $ do
  ar <- thw $ Vec.replicate (len v + len w) 0
  let cp p xs =
        let n = len xs
        in  vcp (mlc p n ar) xs >> return (p+n)
      go p xs ys
        | Vec.null xs = cp p ys
        | Vec.null ys = cp p xs
        |  x  <  y    = wr ar p x >> go p' xs' ys
        |  x  >  y    = wr ar p y >> go p' xs  ys'
        | otherwise   = wr ar p x >> go p' xs' ys'
        where x = hed xs
              y = hed ys
              xs' = tal xs
              ys' = tal ys
              p' = p + 1
  l  <- go 0 v w
  mlc ar 0 l >>= frz
--}

-- vecIdx (SparseVector n ix vs) j = 

-- vecUnion :: (Eq a, Storable a) => Vector a -> Vector a -> Vector a
-- vecUnion v w = Vec.fromList $ Vec.toList v `List.union` Vec.toList w

{--}
------------------------------------------------------------------------------
-- | Add two vectors.
--   NOTE: Assumes that the two vectors are sorted, by ascending indices.
unsafeVecAdd :: (Num a, Storable a) =>
                SparseVector a -> SparseVector a -> SparseVector a
{-# SPECIALIZE unsafeVecAdd :: SpVD -> SpVD -> SpVD #-}
unsafeVecAdd (SparseVector m ix vs) (SparseVector n jx ws)
  = runST $ do
  let s = t + u
      t = len vs
      u = len ws
  ai  <- new s
  ax  <- new s
  let cp p l r kx xs = do
          let o = l - p
          vcp (mlc r o ai) (slc p o kx)
          vcp (mlc r o ax) (slc p o xs)
          return (r+o)
      upd r i x = wr ai r i >> wr ax r x
      go p q r
        | p  ==  t  = cp q u r jx ws
        | q  ==  u  = cp p t r ix vs
        | i  <   j  = upd r i  v    >> go p' q  r'
        | i  >   j  = upd r j    w  >> go p  q' r'
        | otherwise = upd r i (v+w) >> go p' q' r'
        where (p', q', r') = (p+1, q+1, r+1)
              (i, j, v, w) = (ix!p, jx!q, vs!p, ws!q)
  l <- go 0 0 0
  ai' <- frz $ mlc 0 l ai
  ax' <- frz $ mlc 0 l ax
  return $ SparseVector (max m n) ai' ax'
--}

csVecAdd :: (Num a, Storable a) =>
            SparseVector a -> SparseVector a -> SparseVector a
{-# SPECIALIZE csVecAdd :: SpVD -> SpVD -> SpVD #-}
csVecAdd (SparseVector m ix vs) (SparseVector _ jx ws) = runST $ do
  let s = t + u
      t = len ix
      u = len jx
  ai  <- new s
  ax  <- new s
  flg <- thw $! Vec.replicate m (-1)
  let goI !nz !p
        | p < t = do
          let i = ix!p
          wr ai  nz i
          wr ax  nz (vs!p)
          wr flg i  nz
          goI (succ nz) (succ p)
        | otherwise = return nz
  let goJ !nz !p
        | p < u = do
          let j = jx!p
              w = ws!p
              p' = p+1
          f <- rd flg j
          if f >= 0
            then do rd ax f >>= \x -> wr ax f (x+w)
                    goJ nz p'
            else do wr ai nz j
                    wr ax nz w
                    goJ (succ nz) p'
        | otherwise = return nz
  nz' <- goI 0 0
  nnz <- goJ nz' 0
  ai' <- frz $! mlc 0 nnz ai
  ax' <- frz $! mlc 0 nnz ax
  return $! SparseVector m ai' ax'

csVecWith ::
  Storable a =>
  (a -> a -> a) -> SparseVector a -> SparseVector a -> SparseVector a
csVecWith fun (SparseVector 0 ix vs) (SparseVector 0 _ ws) =
  SparseVector 0 ix $ Vec.zipWith fun vs ws
csVecWith fun (SparseVector m ix vs) (SparseVector n jx ws)
  | m  ==  n  = runST $ do
  let s = t + u
      t = len ix
      u = len jx
  ai  <- new s
  ax  <- new s
  flg <- thw $! Vec.replicate m (-1)
  let goI !nz !p
        | p < t = do
          let i = ix!p
          wr ai  nz i
          wr ax  nz (vs!p)
          wr flg i  nz
          goI (succ nz) (succ p)
        | otherwise = return nz
  let goJ !nz !p
        | p < u = do
          let j = jx!p
              w = ws!p
              p' = p+1
          f <- rd flg j
          if f >= 0
            then do rd ax f >>= \x -> wr ax f (x `fun` w)
                    goJ nz p'
            else do wr ai nz j
                    wr ax nz w
                    goJ (succ nz) p'
        | otherwise = return nz
  nz' <- goI 0 0
  nnz <- goJ nz' 0
  SparseVector m <$> slc 0 nnz `fmap` frz ai <*> slc 0 nnz `fmap` frz ax

vecAddMap :: (Num a, Storable a) =>
             SparseVector a -> SparseVector a -> SparseVector a
{-# SPECIALIZE vecAddMap :: SpVD -> SpVD -> SpVD #-}
vecAddMap u v =
  let um = vec2map u
      vm = vec2map v
      wm = IntMap.unionWith (+) um vm
      rn = elemNum u `max` elemNum v
      (ri, ws) = unzip $ IntMap.toList wm
  in  SparseVector rn (Vec.fromList ri) (Vec.fromList ws)

-- | Pre-sorts both vectors, guaranteeing that the addition is well-behaved.
--   NOTE: Slow.
vecAdd :: (Num a, Eq a, Storable a) =>
-- vecAdd :: (Num a, Ord a, Storable a) =>
          SparseVector a -> SparseVector a -> SparseVector a
{-# SPECIALIZE vecAdd :: SpVD -> SpVD -> SpVD #-}
-- vecAdd v w = vecSort v `unsafeVecAdd` vecSort w
vecAdd v w = sparseUsing 0 $ Vec.zipWith (+) (fullUsing 0 v) (fullUsing 0 w)

-- | Scale a sparse vector.
vecScale :: (Num a, Storable a) => a -> SparseVector a -> SparseVector a
{-# SPECIALIZE vecScale :: Double -> SpVD -> SpVD #-}
vecScale a = sparseMap (a*)

------------------------------------------------------------------------------
-- | Sort the given sparse-vector by increasing index.
vecSort :: Storable a => SparseVector a -> SparseVector a
{-# SPECIALIZE vecSort :: SpVD -> SpVD #-}
vecSort (SparseVector rn ri xs) =
  let ix = Vec.toList ri `zip` Vec.toList xs
      f (i,_) (j,_) = compare i j
      (ri', xs') = Vec.fromList *** Vec.fromList <<< unzip $ List.sortBy f ix
  in  SparseVector rn ri' xs'

vecSort' :: Storable a => SparseVector a -> SparseVector a
{-# SPECIALIZE vecSort' :: SpVD -> SpVD #-}
vecSort' (SparseVector rn ri xs) = runST $ do
  ar <- thw $ Vec.enumFromN 0 (len ri)
  let srt i j = compare (ri!i) (ri!j)
  Mut.sortBy srt ar
  ks <- frz ar
  return $ SparseVector rn (ri `bkp` ks) (xs `bkp` ks)
      
------------------------------------------------------------------------------
-- | Eliminate any zero-elements.
vecFilt :: (Num a, Eq a, Storable a) => SparseVector a -> SparseVector a
{-# INLINE vecFilt #-}
vecFilt  = vecFiltBy (/= 0)

-- | Filter the vector, keeping only the elements that satisfy the given
--   predicate.
vecFiltBy :: Storable a => (a -> Bool) -> SparseVector a -> SparseVector a
{-# SPECIALIZE vecFiltBy :: (Double -> Bool) -> SpVD -> SpVD #-}
vecFiltBy f (SparseVector rn ri xs) =
  let ps = Vec.enumFromN 0 (len ri)
      pj = Vec.filter (f . (xs!)) ps
      rj = bkp ri pj
      ys = bkp xs pj
  in  SparseVector rn rj ys


-- * Sparse vector-specific functionality.
------------------------------------------------------------------------------
-- | Sparse-vector dot product.
--   
--   TODO: The `SPEC` specialisations won't fire unless this is inlined?
spdot :: (Storable a, Num a) => SparseVector a -> SparseVector a -> a
spdot (SparseVector m ix xs) (SparseVector n jx ys)
  | m  /=  n  = errDims "spdot" m n
  | otherwise = go SPEC 0 0
  where
    go !_ !p !q | p == n || q == n = 0
                | i  <  j          = go SPEC p' q
                | i  >  j          = go SPEC p q'
                | otherwise        = xs!p*ys!q + go SPEC p' q'
      where
        (i, j, p', q') = (ix!p, jx!q, p+1, q+1)

-- | Vector and sparse-vector dot-product.
vspdot :: (Storable a, Num a) => Vector a -> SparseVector a -> a
vspdot xs (SparseVector n jx ys)
  | len xs /= n = errDims "vspdot" (len xs) n
  | otherwise   = Vec.sum $ Vec.zipWith (*) (xs `bkp` jx) ys
{-# SPECIALIZE   vspdot :: Vector R -> SparseVector R -> R #-}
{-# INLINABLE[2] vspdot #-}

-- | Sparse-vector and vector dot-product.
spvdot :: (Storable a, Num a) => SparseVector a -> Vector a -> a
spvdot (SparseVector m ix xs) ys
  | m /= len ys = errDims "vspdot" m (len ys)
  | otherwise   = Vec.sum $ Vec.zipWith (*) xs (ys `bkp` ix)
{-# SPECIALIZE   spvdot :: SparseVector R -> Vector R -> R #-}
{-# INLINABLE[2] spvdot #-}

------------------------------------------------------------------------------
-- | Element-wise multiplication of a dense and a sparse vector.
vspmul :: (Storable a, Num a) => Vector a -> SparseVector a -> SparseVector a
vspmul xs (SparseVector n jx ys)
  | len xs == n = let zs = Vec.zipWith (\j -> (*) (xs!j)) jx ys
                  in  SparseVector n jx zs
  | otherwise   = errDims "vspmul" (len xs) n
{-# SPECIALIZE   vspmul :: Vector R -> SparseVector R -> SparseVector R #-}
{-# INLINABLE[2] vspmul #-}

errDims :: String -> Int -> Int -> a
errDims fs m =
  error . printf "%s: dimensions (`%d & %d`) don't match" fs m

------------------------------------------------------------------------------
-- | Sum all of the coefficients of the sparse vector.
spsum :: (Storable a, Num a) => SparseVector a -> a
spsum  = Vec.sum . vecData
{-# INLINE spsum #-}


-- * Mixed sparse- and dense- vector operations.
------------------------------------------------------------------------------
-- | Vector addition with a sparse LHS and a dense RHS.
spvpv :: (Num a, Storable a) => SparseVector a -> Vector a -> Vector a
spvpv (SparseVector _ ri xs) ys = acc_ (+) ys ri xs
{-# INLINE[2] spvpv #-}

-- | Vector addition with a dense LHS and a sparse RHS.
vpspv :: (Num a, Storable a) => Vector a -> SparseVector a -> Vector a
vpspv ys (SparseVector _ ri xs) = acc_ (flip (+)) ys ri xs
{-# INLINE[2] vpspv #-}


-- * Additional-/helper- functions
------------------------------------------------------------------------------
cat :: Storable a => SparseVector a -> SparseVector a -> SparseVector a
cat (SparseVector m ix xs) (SparseVector n jx ys) = SparseVector (m+n) kx zs
  where
    kx = ix <> Vec.map (m+) jx
    zs = xs <> ys
{-# INLINE[2] cat #-}


-- * Sparse vectors that are represented using `IntMap's.
------------------------------------------------------------------------------
vec2map :: Storable a => SparseVector a -> IntMap a
vec2map (SparseVector _ ri xs) =
  IntMap.fromList $ Vec.toList ri `zip` Vec.toList xs
{-# SPECIALIZE vec2map :: SpVD -> IntMap Double #-}

map2vec :: Storable a => Int -> IntMap a -> SparseVector a
map2vec rn vm =
  let (ri, xs) = unzip $ IntMap.toList vm
  in  SparseVector rn (Vec.fromList ri) (Vec.fromList xs)
{-# SPECIALIZE map2vec :: Int -> IntMap Double -> SpVD #-}

vmscale :: Num a => a -> IntMap a -> IntMap a
vmscale s = IntMap.map (s*)
{-# INLINE vmscale #-}

vmadd :: Num a => IntMap a -> IntMap a -> IntMap a
vmadd  = IntMap.unionWith (+)
{-# INLINE vmadd #-}


-- * Testing and examples.
------------------------------------------------------------------------------
sparseTest = do
  let v = SparseVector  3 [0,1] [2,4] :: SpV Z
      w = SparseVector  3 [0,2] [3,7] :: SpV Z
      l = SparseVector 22 [0,1,5, 7,11,14,21] [4,-1,7,1,-4, -7,-1] :: SpV Z
      r = SparseVector 22 [2,4,7,11,14,15,16] [-7,-4,-7,-5,7,3,-6] :: SpV Z
{-- }
  print $ v `vecAdd` v
  print $ v `unsafeVecAdd` v
  print $ v `vecAdd` w
  print $ v `unsafeVecAdd` w
  print $ w `vecAdd` v
  print $ w `unsafeVecAdd` v
  print $ w `vecAdd` w
  print $ w `unsafeVecAdd` w
--}

  print $ l
  print $ r
  print $ l `vecAdd` r
  print $ l `unsafeVecAdd` r
