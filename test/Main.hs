{-# LANGUAGE FlexibleContexts, UndecidableInstances #-}
module Main where

import Test.Framework (defaultMain, testGroup)
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.QuickCheck
import qualified Data.List
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec

import Data.Vector.Helpers
import Data.CCSMat as CCS
import Data.CCSMat.Vector (fromSparseCols, vspm, vxspm, vXspm)
import Data.CCSMat.Stream (spmv)
import Data.Vector.Sparse

import Data.Vector.Arbitrary
import Data.CCSMat.Arbitrary


-- * Testing data types
------------------------------------------------------------------------------
-- | Container for a matrix & vector with compatible dimensions.
data MatVec a = MatVec { mat :: CCSMat a, vec :: Vector a }
  deriving (Eq, Show)

data MatSpV a = MatSpV (CCSMat a) (SparseVector a)
  deriving (Eq, Show)

-- TODO:
data VecMatVec a = VecMatVec (Vector a) (CCSMat a) (Vector a)
  deriving (Eq, Show)


-- ** And associated instances
------------------------------------------------------------------------------
instance (Storable a, Arbitrary a) =>
  Arbitrary (MatVec a) where
  arbitrary = sized matvec
    where
      matvec 0 = pure $ MatVec (CCS 0 mempty (Vec.singleton 0) mempty) mempty
      matvec n = MatVec <$> fromSparseCols `fmap` spvecs n
                        <*> Vec.fromList   `fmap` vector n

instance (Storable a, Arbitrary a) =>
  Arbitrary (MatSpV a) where
  arbitrary = sized matspv
    where
      matspv 0 = pure $ MatSpV (CCS 0 mempty (Vec.singleton 0) mempty) mempty
      matspv n = do
        m <- choose (0,n-1)
        MatSpV <$> fromSparseCols `fmap` spvecs n <*> spv n m


-- * Helper functions
------------------------------------------------------------------------------
-- | Construct "arbitrary" sparse vectors of length `n`.
spvecs n = go 0
  where
    go j | j  <  n   = (:) <$> (choose (1,n-1) >>= spv n) <*> go (j+1)
         | otherwise = pure []

spv n m = do ks <- Vec.fromList <$> sequence [choose (0,n-1) | i <- [1..m]]
             xs <- Vec.fromList <$> vector m
             pure $ SparseVector n ks xs

-- | Matrix transpose operator.
tr :: CCSMat Z -> CCSMat Z
tr  = CCS.unsafeTranspose
-- tr  = CCS.transpose
{-# INLINE tr #-}


-- * Required matrix and vector properties
------------------------------------------------------------------------------
prop_vxspm_spmv (MatVec m v) = v `vxspm` m == tr m `spmv` v
prop_vspm_spmv  (MatVec m v) = v `vspm`  m == tr m `spmv` v
prop_vXspm_spmv (MatVec m v) = v `vXspm` m == tr m `spmv` v
prop_vXspm_vspm (MatVec m v) = v `vXspm` m == v `vspm` (m :: CCSMat Z)


-- * Test framework using QuickCheck tests
------------------------------------------------------------------------------
vectorTests = [ testProperty "prop_vxspm_spmv" prop_vxspm_spmv
              , testProperty "prop_vspm_spmv " prop_vspm_spmv
              , testProperty "prop_vXspm_spmv" prop_vXspm_spmv
              , testProperty "prop_vXspm_vspm" prop_vXspm_vspm
              ]

main =
  defaultMain 
  [ testGroup "CCSMat.Vector" vectorTests
  ]
