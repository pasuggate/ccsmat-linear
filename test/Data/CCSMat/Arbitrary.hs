{-# LANGUAGE TupleSections #-}
{-# LANGUAGE StandaloneDeriving, GeneralizedNewtypeDeriving, DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts, UndecidableInstances #-}
module Data.CCSMat.Arbitrary where

------------------------------------------------------------------------------
--
--  Generate arbitrary elements of type `CCSMat a`.
--
--  Changelog:
--   + 20/08/2015  --  initial file;
--

import Prelude hiding (splitAt)
import Control.Arrow
import GHC.Generics
import GHC.Word
import qualified Data.List as L
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Generic as G
import Test.QuickCheck

import Data.Common
import Data.CCSMat.Internal


-- * Wrapper-types, for the various tests.
------------------------------------------------------------------------------
newtype Mat a = Mat  { runMat  :: CCSMat a }

-- | Some convenience types.
type Mat8 = Mat Word8
type MatC = Mat Char
type MatZ = Mat Int
type MatQ = Mat Rational
type MatD = Mat Double

data MatPair a = MatPair (CCSMat a) (CCSMat a)
               deriving (Eq, Show, Generic)

type MatPairZ  = MatPair Int
type MatPairD  = MatPair Double

data Rows  = Rows !Int !IVec
type Cols  = Rows


-- * Instances needed for QuickCheck.
------------------------------------------------------------------------------
-- deriving instance (Storable a, Num a, Show    a) => Show    (Mat a)
deriving instance (Storable a, Num a, Eq      a) => Eq      (Mat a)
deriving instance (Storable a, Num a, Generic a) => Generic (Mat a)
deriving instance Arbitrary (CCSMat a) => Arbitrary (Mat a)

instance (Storable a, Show a) => Show (Mat a) where
  show (Mat m) = show m

instance (Storable a, Num a, Eq a, Arbitrary a) => Arbitrary (CCSMat a) where
  arbitrary = sized mat where
    mat 0 = pure $ CCS 0 mempty (vec [0]) mempty
--     mat 0 = mat 1
    mat 1 = CCS 1 (vec [0]) (vec [0,1]) . Vec.singleton <$> nzelem
    mat 2 = CCS 2 (vec [0,1]) (vec [0,1,2]) . vec <$> vectorOf 2 nzelem
    mat n = do
      (rn, cs) <- counts n
      cp <- colptr cs
      ri <- rowidx rn cs
      CCS rn ri cp <$> values n

deriving instance Eq      Rows
deriving instance Show    Rows
deriving instance Generic Rows

instance Arbitrary Rows where
  arbitrary = sized rows where
    rows 0 = pure $! Rows 0 mempty
    rows 1 = pure $! Rows 1 (Vec.singleton 0)
    rows n = do
      (rn, cs) <- chunks n
      pure . Rows rn . Vec.map pred $ vec cs

instance (Storable a, Num a, Eq a, Arbitrary a) => Arbitrary (MatPair a) where
  arbitrary = MatPair <$> sized mat <*> sized mat
    where
      mat 0 = pure $ CCS 0 mempty (vec [0]) mempty
      mat 1 = CCS 1 (vec [0]) (vec [0,1]) . Vec.singleton <$> nzelem
      mat 2 = CCS 2 (vec [0,1]) (vec [0,1,2]) . vec <$> vectorOf 2 nzelem
      mat n = do
        cs <- mkrows n
        cp <- colptr cs
        ri <- rowidx n cs
        CCS n ri cp <$> values (Vec.last cp)


-- * Various helper-functions, for building arbitrary, CCS sparse-matrices,
--   for testing with `QuickCheck`.
------------------------------------------------------------------------------
-- | Generate (unsorted) row-indices.
rowidx :: Int -> [Int] -> Gen IVec
rowidx m cs = vec . concat <$> mapM (takeN m) cs

-- | Generates a vector of column-pointers into the row- & element- data.
colptr :: [Int] -> Gen IVec
colptr  = pure . vec . L.scanl (+) 0

------------------------------------------------------------------------------
-- | Break `n` non-zero elements into a few elements per row/column.
chunks :: Int -> Gen (Int, [Int])
chunks n = do
  let up   = uncurry max . (2 :: Int,) . round . log . fi
      go s = choose (1, s)
  rc <- ceiling . (fi n/) . fi <$> choose (2, up n)
  (rc,) . ([1..rc] ++) <$> vectorOf (n-rc) (go rc)

counts :: Int -> Gen (Int, [Int])
counts n =
  let cnt r = check r . pack . L.sort
  in  (length &&& id <<< uncurry cnt) <$> chunks n

check :: Int -> [Int] -> [Int]
check m   [n]
  | n  >  m   = [m, n-m]
  | otherwise = [n]
check m (n:ns)
  | n  <  m   = n:check m ns
  | otherwise = m:check m ns'
  where ns'   = (head ns+n-m):tail ns

-- | Make `n` rows, with about 10% fill-rate.
mkrows :: Int -> Gen [Int]
mkrows 0 = return []
mkrows 1 = return [0]
mkrows n = do
  let nnz = max 2 $ (n-1) `div` 5 + 1
  vectorOf n (choose (2, nnz))

pack :: (Eq a, Num a) => [a] -> [a]
{-# SPECIALIZE pack :: [Int] -> [Int] #-}
pack    []  = []
pack (n:ns) =
  let go c _    []  = [c]
      go c x (y:ys)
        | x  ==  y  = go (c+1) x ys
        | otherwise = c:go 1 y ys
  in  go 1 n ns
