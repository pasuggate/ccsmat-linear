{-# LANGUAGE FlexibleInstances, GADTs #-}
module Data.Vector.Arbitrary where

import Test.QuickCheck

import qualified Data.Vector.Generic as Gen
import Data.Vector.Storable (Storable, Vector)
import qualified Data.Vector.Storable as Vec

import Control.Monad (foldM, foldM_, zipWithM, zipWithM_)
-- import Control.Monad.Trans.Writer
import Data.Function (on)
import Data.Functor.Identity
import Data.List (sortBy)
import Data.Monoid
import Data.Maybe (catMaybes)


instance (Arbitrary a, Storable a) => Arbitrary (Vector a) where
  arbitrary = fmap Vec.fromList arbitrary

instance (CoArbitrary a, Storable a) => CoArbitrary (Vector a) where
  coarbitrary = coarbitrary . Vec.toList
