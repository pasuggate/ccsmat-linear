module Data.Common where

------------------------------------------------------------------------------
--
--  Generate arbitrary elements of type `CCSMat a`.
--
--  Changelog:
--   + 20/08/2015  --  initial file;
--

import Prelude hiding (splitAt)
import Control.Arrow
import GHC.Generics
import GHC.Word
import qualified Data.List as L
import Data.Set (Set, elemAt, deleteAt)
import qualified Data.Set as Set
import Data.Vector.Helpers
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Generic as G
import Test.QuickCheck


-- | Generate arbitrary, non-zero elements.
nzelem :: (Eq a, Num a, Arbitrary a) => Gen a
nzelem  = suchThat arbitrary (/= 0)

-- | Randomly choose `n` elements, of a set containing `m` elements.
takeN :: Int -> Int -> Gen [Int]
takeN m 0 = pure []
takeN m n =
  let m' = pred m
      go  _ _ 0 = pure []
      go ys k l = do
        (y, ys') <- flip splitAt ys <$> choose (0, k)
        (y:) <$> go ys' (pred k) (pred l)
  in  go (Set.fromList [0..m']) m' n

splitAt :: Int -> Set a -> (a, Set a)
splitAt i xs = (elemAt i xs, deleteAt i xs)

-- | Generates arbitrary, non-zero values, for test-matrices.
values :: (Num a, Eq a, Arbitrary a, Storable a) => Int -> Gen (Vector a)
values  = fmap vec . flip vectorOf nzelem

-- | Generates arbitrary values, for test-matrices, and these are allowed to
--   be zero.
valuez :: (Num a, Eq a, Arbitrary a, Storable a) => Int -> Gen (Vector a)
valuez  = fmap vec . flip vectorOf arbitrary


-- * Some convenience-aliases.
------------------------------------------------------------------------------
fi :: (Integral i, Num a) => i -> a
{-# INLINE fi #-}
fi  = fromIntegral
