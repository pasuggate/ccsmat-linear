{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, TupleSections,
             FlexibleContexts, BangPatterns, RecordWildCards, OverloadedStrings,
             MultiParamTypeClasses, FunctionalDependencies,
             FlexibleInstances, TypeSynonymInstances, InstanceSigs,
             GeneralizedNewtypeDeriving, StandaloneDeriving,
             DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric,
             GADTs, KindSignatures, DataKinds, PolyKinds,
             TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances,
             CPP, TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Numeric.Solver.Iterative
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Some iterative solvers.
--
-- NOTE:
-- 
-- Changelog:
--  + 28/06/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Numeric.Solver.Iterative where

import Data.List (tails)
import Data.Foldable (toList)
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import Linear
import Text.Printf

import Numeric.LinearAlgebra ((#>), (<#))
import qualified Numeric.LinearAlgebra as H
import qualified Numeric.LinearAlgebra.Data as H

import Data.Vector.Linear
import Data.Vector.Helpers
import Data.CCSMat (CCSMat(..))
import qualified Data.CCSMat as CCS
import qualified Data.CCSMat.Multiply as CCS
-- import qualified Data.CCSMat.Sort as CCS
import qualified Data.CCSMat.Stream as CCS
import qualified Data.CCSMat.MapMat as CCS
import qualified Data.CCSMat.Submatrix as CCS
import qualified Data.CCSMat.RowCol as CCS
import qualified Data.CCSMat.Assemble as Asm
import qualified Data.CCSMat.Constrain as Con
import qualified Data.CCSMat.Vector as SpV


-- * Krylov-subspace iterative solvers.
------------------------------------------------------------------------------
cg' :: R -> Z -> CCSMat R -> Vector R -> Vector R -> Vector R
cg' eps n mat rhs lhs =
  let go r p k x
        | sqrt t < eps = x
        | k     >=  n  = x
        | otherwise    = go r' p' (k+1) x'
        where
          q  = mat `CCS.spmv` p
          s  = r `H.dot` r
          a  = s / p `H.dot` q
          x' = x + a `H.scale` p
          r' = r - a `H.scale` q
          t  = r' `H.dot` r'
          b  = t / s
          p' = r' + b `H.scale` p
      r0 = rhs - mat `CCS.spmv` lhs
  in  go r0 r0 0 lhs

cgs :: R -> Z -> CCSMat R -> Vector R -> Vector R -> [Vector R]
cgs eps n mat rhs lhs =
  let go r p k x
        | sqrt t < eps = []
        | k     >=  n  = []
        | otherwise    = x:go r' p' (k+1) x'
        where
          q  = mat `CCS.spmv` p
          s  = r `H.dot` r
          a  = s / p `H.dot` q
          x' = x + a `H.scale` p
          r' = r - a `H.scale` q
          t  = r' `H.dot` r'
          b  = t / s
          p' = r' + b `H.scale` p
      r0 = rhs - mat `CCS.spmv` lhs
  in  go r0 r0 0 lhs


-- * Additional solvers.
------------------------------------------------------------------------------
-- | One weighted Jacobi-iteration step; i.e., compute:
--   
--     u_{n+1}  =  (1-w)u_n + wD^{-1}[b - Ru_n] ;
--   
--   where:
--   
--     A  =  D + R ,    D  =  diag{A} .
--   
jacobi :: R -> CCSMat R -> Vector R -> Vector R -> Vector R
jacobi w matA rhs lhs =
  let diag = CCS.takeDiag matA
      rest = CCS.dropDiag matA
  in  jacobi' w diag rest rhs lhs

jacobi' :: R -> Vector R -> CCSMat R -> Vector R -> Vector R -> Vector R
jacobi' w vecD matR rhs lhs =
  let brak = rhs - matR `CCS.spmv` lhs
      vec' = Vec.map recip vecD
  in  H.scale (1-w) lhs + H.scale w (vec' * brak)

------------------------------------------------------------------------------
-- | Modified Richardson Iteration.
richie :: R -> CCSMat R -> Vector R -> Vector R -> Vector R
richie w matA rhs lhs =
  let res = matA `CCS.spmv` lhs - rhs
  in  lhs - H.scale w res


-- * Polynomial-acceleration functions.
------------------------------------------------------------------------------
-- | Evaluate the Shanks Transformation for the given series.
--   TODO: Find a numerically-stable evaluation ordering?
shanks :: Fractional a => [a] -> [a]
{-# SPECIALISE shanks :: [Vector R] -> [Vector R] #-}
shanks (u:v:w:xs) =
  let x = w - (w-v) * (w-v) / ((w-v) - (v-u)) -- Wikipedia
--   let x = (u*w - v*v) / (u - (v+v) + w)
--   let x = (u*w - v*v) / (u - v - v + w)
  in  x:shanks (v:w:xs)

------------------------------------------------------------------------------
-- | Richard Extrapolation.
--   
--   FIXME: Why do I need to scale by 2^{-n} ?
extrap :: Fractional a => Z -> [a] -> [a]
extrap n xs =
  let go (k:ks') (d:ds') i (x:xs') =
        let x' = d*x*fi (i+k)^^n*(-1)^^(k+n)
        in  x':go ks' ds' (i+1) xs'
      go     _       _   _     _   = []
      (ks, ds) = ([0..n], recip . fi <$> bins n 0)
      fi = fromIntegral
      s = 2^^(-n)
  in  map (s*) $ zipWith ((sum .) . go ks ds) [1..] $ tails xs


-- ** Helper functions.
------------------------------------------------------------------------------
bins :: Integral a => a -> a -> [a]
bins n k | k  <=  n  = fac k*fac (n-k):bins n (k+1)
         | otherwise = []

fac :: Integral i => i -> i
fac 0 = 1
fac 1 = 1
fac n = n*fac (n-1)
